const firebase = require("firebase");
require("firebase/firestore");

const { firebaseData } = require("./fs-export.js");

const firebaseConfig = {
  apiKey: "AIzaSyAwOzDMePmmwEcd48bCubNhqZ9EoNSa8pE",
  authDomain: "appelasoma.firebaseapp.com",
  databaseURL: "https://appelasoma.firebaseio.com",
  projectId: "appelasoma",
  storageBucket: "appelasoma.appspot.com",
  messagingSenderId: "27981024048",
  appId: "1:27981024048:web:9fbd7f9fbc103139c256f5",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firestore = firebaseApp.firestore();

firestore.settings({
  host: "localhost:8080",
  ssl: false,
});

async function importDB() {
  try {
    const setDoc = async (path, data) => {
      return firestore.doc(path).set(data);
    };

    console.log("Iniciando população do banco de dados de desenvolvimento");

    for (const [colName, colData] of Object.entries(firebaseData)) {
      for (const [docId, docData] of Object.entries(colData.docs)) {
        await setDoc(`${colName}/${docId}`, docData);

        if (docData.collections) {
          for (const [docCollectionId, docCollectionData] of Object.entries(
            docData.collections.cities
          )) {
            await setDoc(
              `${colName}/${docId}/cities/${docCollectionId}`,
              docCollectionData
            );
          }
        }

        console.log(`collection "${colName}" populada`);
      }
    }

    console.log("Finalizou importação de dados");
    return 0;
  } catch (e) {
    console.log(e);
  }
}

importDB().then(() => ({}));
