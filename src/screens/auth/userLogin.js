import React, { useEffect } from "react";
import styled from "styled-components";
import { Card, Col, Row, Typography } from "antd";
import { push } from "connected-react-router";
import UserSignInForm from "../../containers/auth/userSignInForm";
import backgroundImage from "../../assets/imgs/bg.svg";
import somaLogo from "../../assets/imgs/soma_logo.svg";
import logo from "../../assets/imgs/logo.png";
import { useSelector } from "react-redux";
import { getAuthState } from "../../services/auth";
import { store } from "../../store";
import { getUserFromStore } from "../../helpers/store";

const { Title } = Typography;
const Container = styled.div`
  background-color: #090C1D;
  background-position: bottom;
  background-size: cover;
  background-repeat: no-repeat;
  min-height: 100vh;
`;

const LogoContainer = styled.div`
  margin-bottom: 30px;
`;

const CustomCard = styled(Card)`
  padding: 20px 50px;
  border-radius: 10px;
`;

export default () => {
  const { isLoggedIn } = useSelector((state) => state.auth);

  useEffect(() => {
    getAuthState();
  }, []);

  if (isLoggedIn) {
    const user = getUserFromStore();
    console.log(user);
    if(user._isSuspended){
      store.dispatch(push("/web/user_profile"));
    }
    if(!user._name || user._name === ""){
      store.dispatch(push("/web/user_profile"));
    }
    if(!user._isSuspended){
      store.dispatch(push("/web/news_feed"));  
    }

  }
  return (
    <Container>
      <Row style={styles.row} type="flex" justify="center" align="middle">
        <Col style={styles.formContainer}>
          <LogoContainer>
            <img src={somaLogo} alt="Marca da Qualitare" />
          </LogoContainer>
        <UserSignInForm />
        </Col>
      </Row>
    </Container>
  );
};
const styles = {
  row: {
    height: "100vh",
  },
  formContainer: {
    textAlign: "center",
  },
};
