import React, { useEffect } from "react";
import styled from "styled-components";
import { Card, Col, Row, Typography } from "antd";
import { push } from "connected-react-router";
import SignInForm from "../../containers/auth/signInForm";
import backgroundImage from "../../assets/imgs/bg.svg";
import logo from "../../assets/imgs/logo.png";
import { useSelector } from "react-redux";
import { getAuthState } from "../../services/auth";
import { store } from "../../store";

const { Title } = Typography;
const Container = styled.div`
  background-image: url(${backgroundImage});
  background-color: #2b2b2b;
  background-position: bottom;
  background-size: cover;
  background-repeat: no-repeat;
  min-height: 100vh;
`;

const LogoContainer = styled.div`
  margin-bottom: 30px;
`;

const CustomCard = styled(Card)`
  padding: 20px 50px;
  border-radius: 10px;
`;

export default () => {
  const { isLoggedIn } = useSelector((state) => state.auth);

  useEffect(() => {
    getAuthState();
  }, []);

  if (isLoggedIn) {
    store.dispatch(push("/dashboard/home"));
  }
  return (
    <Container>
      <Row style={styles.row} type="flex" justify="center" align="middle">
        <Col style={styles.formContainer}>
          <LogoContainer>
            <img src={logo} alt="Marca da Qualitare" />
          </LogoContainer>
          <CustomCard>
            <Title>Iniciar Sessão</Title>

            <SignInForm />
          </CustomCard>
        </Col>
      </Row>
    </Container>
  );
};
const styles = {
  row: {
    height: "100vh",
  },
  formContainer: {
    textAlign: "center",
  },
};
