import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddEventForm from "../../containers/events/AddEventForm";
import EventsList from "../../containers/events/EventsList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Eventos ativos"
      pageHeaderIcon="snippets"
      headerTitle="Eventos"
      extras={[
        <Can
          key="add_events"
          perform="events:add"
          yes={() => <AddEventForm />}
        />,
      ]}
    >
      <EventsList />
    </Container>
  );
};
