import React from "react";
import Can from "../../rbac/Can";
import UserContainer from "../../components/UserContainer";
import AddEventForm from "../../containers/events/AddEventForm";
import EventsList from "../../containers/events/feed/EventsList";

export default () => {
  return (
    <UserContainer
      pageHeaderIcon="snippets"
      headerTitle="Eventos"
    >
      <EventsList/>
    </UserContainer>
  );
};
