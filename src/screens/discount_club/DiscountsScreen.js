import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddDiscountForm from "../../containers/discounts_club/AddDiscountForm";
import DiscountsList from "../../containers/discounts_club/DiscountsList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Descontos ativos"
      pageHeaderIcon="snippets"
      headerTitle="Clube de Benefícios"
      extras={[
        <Can
          key="add_events"
          perform="events:add"
          yes={() => <AddDiscountForm />}
        />,
      ]}
    >
      <DiscountsList />
    </Container>
  );
};
