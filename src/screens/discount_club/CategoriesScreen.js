import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddCategoryForm from "../../containers/discounts_club/AddCategoryForm";
import CategoriesList from "../../containers/discounts_club/CategoriesList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Categorias do Club de Benefícios"
      pageHeaderIcon="tags"
      headerTitle="Clube de Benefícios"
      extras={[
        <Can
          key="users"
          perform="news:interests_area:add"
          yes={() => <AddCategoryForm />}
        />,
      ]}
    >
      <CategoriesList />
    </Container>
  );
};
