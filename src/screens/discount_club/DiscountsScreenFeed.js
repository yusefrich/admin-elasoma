import React from "react";
import Can from "../../rbac/Can";
import UserContainer from "../../components/UserContainer";
import AddDiscountForm from "../../containers/discounts_club/AddDiscountForm";
import DiscountsList from "../../containers/discounts_club/feed/DiscountsList";

export default () => {
  return (
    <UserContainer
      style={{background: "#151A2E !important"}}
      pageHeaderIcon="snippets"
      headerTitle="Descontos"
    >
      <DiscountsList />
    </UserContainer>
  );
};
