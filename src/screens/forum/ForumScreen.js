import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddForumPostForm from "../../containers/forum/AddForumPostForm";
import ForumPostsList from "../../containers/forum/ForumPostsList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Postagens do Fórum"
      pageHeaderIcon="snippets"
      headerTitle="Fórum"
      extras={[
        <Can
          key="add_forum_post"
          perform="forum:post:add"
          yes={() => <AddForumPostForm />}
        />,
      ]}
    >
      <ForumPostsList />
    </Container>
  );
};
