import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddCategoryForm from "../../containers/forum/AddCategoryForm";
import ForumCategoriesList from "../../containers/forum/ForumCategoriesList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Categorias do Fórum"
      pageHeaderIcon="snippets"
      headerTitle="Fórum"
      extras={[
        <Can
          key="add_forum_category"
          perform="forum:categories:add"
          yes={() => <AddCategoryForm />}
        />,
      ]}
    >
      <ForumCategoriesList />
    </Container>
  );
};
