import React from "react";
import Container from "../../components/Container";
import NewsletterSubscribersList from "../../containers/notifications/NewsletterSubscribersList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Assinantes das Newsletters"
      pageHeaderIcon="notification"
      headerTitle="Newsletters"
    >
      <NewsletterSubscribersList />
    </Container>
  );
};
