import React from "react";
import Can from "../../rbac/Can";
import UserContainer from "../../components/UserContainer";
import AddNewsForm from "../../containers/news/AddNewsForm";
import NewsList from "../../containers/news/feed/NewsList";

export default () => {
  return (
    <UserContainer
      pageHeaderIcon="snippets"
      headerTitle="Notícias"
    >
      <NewsList />
    </UserContainer>
  );
};
