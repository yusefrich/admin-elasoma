import React from "react";
import Can from "../../rbac/Can";
import Container from "../../components/Container";
import AddInterestAreaForm from "../../containers/news/AddInterestAreaForm";
import InterestsAreaList from "../../containers/news/InterestsAreaList";

export default () => {
  return (
    <Container
      pageHeaderSubtitle="Áreas de Interesse"
      pageHeaderIcon="ordered-list"
      headerTitle="Áreas de Interesse"
      extras={[
        <Can
          key="users"
          perform="news:interests_area:add"
          yes={() => <AddInterestAreaForm />}
        />,
      ]}
    >
      <InterestsAreaList />
    </Container>
  );
};
