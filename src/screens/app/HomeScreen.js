import React from "react";
import { Button, Layout } from "antd";
import Can from "../../rbac/Can";
import { getUserFromStore } from "../../helpers/store";

const { Content } = Layout;

export default () => {
  const user = getUserFromStore();

  return (
    <Content
      style={{
        margin: "24px 16px",
        padding: 24,
        background: "#fff",
        minHeight: 280,
      }}
    >
      <div>
        <span>{user.name}</span>
        <Can
          perform="dashboard:button:admin"
          yes={() => <Button type="primary">Exibido apenas para ADMIN</Button>}
        />
      </div>
      <div>
        <Can
          perform="dashboard:button:super_admin"
          yes={() => (
            <Button type="secondary">Exibido apenas para SUPER_ADMIN</Button>
          )}
        />
      </div>
    </Content>
  );
};
