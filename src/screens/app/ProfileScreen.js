import React from "react";
import { getUserFromStore } from "../../helpers/store";
import FormEditLoggedProfile from "../../containers/forms/FormEditLoggedProfile";
import Container from "../../components/Container";
import User from "../../models/userModel";
import UpdateableEmail from "../../containers/App/UpdateableEmail";
import Spin from "../../components/Spin";

export default () => {
  const user = getUserFromStore();

  if (!(user instanceof User)) return <Spin />;

  return (
    <>
      <Container headerTitle="Meu Perfil">
        <FormEditLoggedProfile />
      </Container>
      <Container>
        <h3>Alterar Email</h3>
        <UpdateableEmail />
      </Container>
    </>
  );
};
