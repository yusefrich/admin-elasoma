import React, { useEffect, useState } from "react";
import { Select } from "antd";
import { useSelector } from "react-redux";

const { Option } = Select;

export default ({ onSelect, initialValue = null, disabled = false }) => {
  const [selectedState, setSelectedState] = useState(initialValue);
  const { states } = useSelector((store) => store.states);

  useEffect(() => {
    if (initialValue) {
      setSelectedState(initialValue);
      onSelect(initialValue);
    }
  }, [initialValue]);

  const onSelectState = (stateId) => {
    setSelectedState(stateId);
    onSelect(stateId);
  };

  return (
    <Select
      value={selectedState}
      placeholder="Filtrar por estado"
      style={{ width: 220 }}
      onChange={onSelectState}
      disabled={disabled}
      showSearch
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      <Option value={null}>Selecionar Estado</Option>
      {states.map((state) => (
        <Option
          key={state.id}
          value={state.id}
        >{`${state.name}-${state.uf}`}</Option>
      ))}
    </Select>
  );
};
