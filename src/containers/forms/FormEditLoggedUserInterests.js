import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Form, Input, Row, Select } from "antd";

import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateUserService } from "../../services/user";
import { StateActions } from "../../ducks/actions";
import { getCitiesService } from "../../services/state";
import { getAreasOfInterestsService } from "../../services/interests";
import styled from "styled-components";

const InterestHolder = styled.button`

justify-content: center;
align-items: center;
height: 70px;
width: 100%;
margin: 10px;
border-radius: 5px;
background: #0F4DFF;
overflow: hidden;
padding: 0;
border: none;
box-shadow: none;
cursor: pointer;
`;

const InterestTextHolder = styled.p`
  position: absolute;
  top: 50%;
  left: 0;
  right: 0;
  transform: translateY(-50%);
`;
const InterestText = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 18px;
line-height: 175.9%;
margin-bottom: 0;
  color: #fff
`;

const { Option } = Select;

export default Form.create({ name: "form_edit_logged_profile" })(
  ({ form: { getFieldDecorator, validateFieldsAndScroll } }) => {
    const dispatch = useDispatch();
    const [loading, isLoading] = useState(false);
    const [cities, setCities] = useState([]);
    const { states } = useSelector((store) => store.states);
    const [areas, setAreas] = useState([]);

    const user = getUserFromStore();

    useEffect(() => {
      getAreas();
      getStates();
      if(user.state){
        getCities(user.state.id);
      }
      console.log("user data");
      console.log(user);
    }, [user]);

    const saveInterests = async () => {
      try {
        isLoading(true);
        const interests = areas.filter(area => area.selected);
        if (interests.length > 0) {
          await updateUserService(user.id, { interests });
          notifySuccess({
            title: 'Sucesso',
            message: 'Dados salvos com sucesso',
          });

        } else {
          notifyError({
            title: 'Atenção',
            message: 'Selecione no mínimo uma área de interesse',
          });
        }
        isLoading(false);
      } catch (e) {
        isLoading(false);
        notifyError({
          title: 'Erro',
          message: 'Erro ao salvar os novos items da área de interesse',
        });

        console.log("error in get areas");
        console.log(e)

      }
    };

    const getAreas = async () => {
      try {
        let areas = await getAreasOfInterestsService();
        if (user.interests) {
          user.interests.forEach(s => {
            areas = areas.map(area => {
              if (area.id === s.id) return { ...area, selected: s.selected };
              return area;
            });
          });
        }
        setAreas(areas);
      } catch (e) {
        console.log("error in get areas");
        console.log(e)
      }
    };

    const getStates = async () => {
      dispatch(StateActions.getStates());
    };

    const getCities = async (stateId) => {
      if (stateId !== user.state.id) user.city = "";
      const cities = await getCitiesService(stateId);
      setCities(cities);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await updateUserService(user.id, values);
            notifySuccess({
              title: "Tudo certo",
              message: "Os dados foram atualizados",
            });
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const selectImage = s => {
      setAreas(
        areas.map(area => {
          if (area.id === s.id) return { ...area, selected: !s.selected };
          return area;
        }),
      );

      console.log("areas setted");
      console.log(areas)
      console.log(s)
    };
    const InterestArea = ({ area, onClick }) => {
      return (
        <div onClick={() => onClick(area)}>
          <InterestHolder>
            <img style={{
              borderRadius: 5,
              objectFit: 'cover',
              height: 70,
              width: '100%',
              opacity: area.selected ? 0.6 : 1
            }}
              src={area.image}
              alt="Imagem de interesse"
            />

            <InterestTextHolder>
              <InterestText >
                {area.name.charAt(0).toUpperCase()}
                {area.name.slice(1).toLowerCase()}
              </InterestText>
            </InterestTextHolder>
          </InterestHolder>
        </div>
      );
    };

    return (
      <>


        <Row gutter={24}>
          {areas.map(area => (
            <Col span={12}>
              <InterestArea onClick={selectImage} key={area.id} area={area} />
            </Col>

          ))}
        </Row>
        <Button
          loading={loading}
          onClick={saveInterests}
          style={{marginTop: 30, marginLeft: 15, color: '#ffffff', border: 'none'}}
          className="login-form-button"
        >
          Salvar
        </Button>
      </>
    );
  }
);
