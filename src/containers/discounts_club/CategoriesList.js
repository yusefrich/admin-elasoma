import React, { useEffect, useState } from "react";
import { Col, Icon, Popconfirm, Row, Table, Tag, Upload } from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import UpdateableItem from "../../components/UpdateableItem";
import { beforeUpload } from "../../helpers/forms";
import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import {
  createDiscountClubCategoriesListener,
  deleteDiscountClubCategoryService,
  updateDiscountClubCategoryService,
} from "../../services/discountClub";

export default () => {
  const [categories, setCategories] = useState("");
  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getCategories();
  }, []);

  const getCategories = async () => {
    try {
      isLoading(true);
      createDiscountClubCategoriesListener((categories) => {
        setCategories(categories);
        isLoading(false);
      });
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Categoria",
      dataIndex: "name",
      key: "name",
      width: "50%",
    },
    {
      title: "Ícone",
      dataIndex: "icon",
      key: "icon",
      render: (text) => <img src={text} width={30} />,
    },
    {
      title: "",
      dataIndex: "",
      key: "",
      render: (category) => (
        <DeleteAreaButton onDelete={getCategories} category={category} />
      ),
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col>
          <Table
            columns={columns}
            dataSource={categories}
            loading={loading}
            rowExpandable
            expandedRowRender={(category) => (
              <CategoryDetails onUpdate={getCategories} category={category} />
            )}
          />
        </Col>
      </Row>
    </div>
  );
};

const CategoryDetails = ({ category, onUpdate }) => {
  const [loadingName, setLoadingName] = useState(false);
  const [uploadingImage, isUploadingImage] = useState(false);

  const updateArea = async (value) => {
    try {
      setLoadingName(true);
      await updateDiscountClubCategoryService(category, value);
      onUpdate();
      notifySuccess({ title: "Tudo certo", message: "Categoria Atualizada" });
      setLoadingName(false);
    } catch (e) {
      notifyError(e);
      setLoadingName(false);
    }
  };

  const onUpload = async ({ onSuccess, onError, file, onProgress }) => {
    const onSuccessCallback = async (icon) => {
      deleteImageFromUrlService(category.icon);
      updateArea({ icon });
      onSuccess("done");
    };
    uploadFileService({
      url: `discounts_club_categories`,
      onSuccessCallback,
      onError,
      file,
      onProgress,
    });
  };

  const handleChangeImage = (info) => {
    if (info.file.status === "uploading") {
      isUploadingImage(true);
      return;
    }
    if (info.file.status === "done") {
      isUploadingImage(false);
    }
  };

  return (
    <>
      <Row gutter={16}>
        <Col span={24}>
          <h4>Detalhes da Categoria</h4>
        </Col>
        <Col span={6}>
          <span>Nome: </span>
          <UpdateableItem
            type="input"
            field="name"
            loading={loadingName}
            onChange={updateArea}
            value={category.name}
          />
          <br />
          <small>Clique na categoria para alterar </small>
        </Col>
        <Col span={6}>
          <span>Ícone: </span>

          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={beforeUpload}
            customRequest={onUpload}
            onChange={handleChangeImage}
          >
            {category.icon ? (
              <img src={category.icon} alt="avatar" style={{ width: "100%" }} />
            ) : (
              <div>
                <Icon type={uploadingImage ? "loading" : "plus"} />
                <div className="ant-upload-text">Upload</div>
              </div>
            )}
          </Upload>
        </Col>
      </Row>
    </>
  );
};

const DeleteAreaButton = ({ category, onDelete }) => {
  const [loading, setLoading] = useState(false);

  const deleteCategory = async () => {
    try {
      setLoading(true);
      await deleteDiscountClubCategoryService(category);
      onDelete();
      notifySuccess({ title: "Tudo certo", message: "Categoria removida" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Todos descontos relacionados a
          esta categoria
          <br /> também serão excluídos
        </p>
      }
      onConfirm={deleteCategory}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red"> {loading && <Icon type="loading" />} excluir</Tag>
    </Popconfirm>
  );
};
