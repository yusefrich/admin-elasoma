import React, { useEffect, useState } from "react";
import {
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Row,
  Table,
  Spin,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../../helpers/notifications";
import Can from "../../../rbac/Can";
import UserStateFilter from "../../forms/UserStateFilter";
import {
  createDiscountsListenerService,
  deleteDiscountService,
} from "../../../services/discountClub";
import ShowDiscounts from "../ShowDiscounts";
import UserDiscountClubCategoriesFilter from "../UserDiscountClubCategoriesFilter";

export default () => {
  const [discounts, setDiscounts] = useState(null);
  const [loading, isLoading] = useState(false);
  const [selectedState, setSelectedState] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);

  useEffect(() => {
    getDiscounts();
  }, [selectedState, selectedCategory]);

  const getDiscounts = async () => {
    try {
      isLoading(true);

      createDiscountsListenerService(
        { filters: {  categoryId: selectedCategory, stateId: selectedState, disabled: false } },
        (discounts) => {
          setDiscounts(discounts);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Desconto",
      dataIndex: "discount",
      key: "discount",
      width: "15%",
    },
    {
      title: "Descrição",
      key: "discount_description",
      dataIndex: "discount_description",
      width: "20%",
    },
    {
      title: "Local",
      key: "company",
      dataIndex: "company",
      width: "10%",
    },
    {
      title: "Categoria",
      key: "categoryDetails.name",
      dataIndex: "categoryDetails.name",
      width: "15%",
    },
    {
      title: "Endereço",
      key: "address",
      width: "30%",
      render: (data) => (
        <>
          <p>{data.place}</p>
          <small>{data.address}</small>
        </>
      ),
    },
    {
      title: "Estado",
      dataIndex: "stateDetails.uf",
      key: "stateDetails.uf",
    },
    {
      title: "Ações",
      key: "id",
      render: (discount) => (
        <>
          <ShowDiscounts discount={discount} />
          <DeleteDiscountButton discount={discount} />
        </>
      ),
    },
  ];
  const DiscountList = () => (
    <>
      {discounts.map((row) => (
        <ShowDiscounts key={row.id} discount={row} />

      ))}
    </>
  );

  return (
    <div>
      <Row gutter={24}>
        <Col span={4}>
              <UserStateFilter onSelect={(stateId) => setSelectedState(stateId)} />
        </Col>
      </Row>
      <Row gutter={24}>
        <Col span={15}>
              <UserDiscountClubCategoriesFilter
                onSelect={(category) => setSelectedCategory(category)}
              />
        </Col>
      </Row>
      <div hidden={!loading} style={{textAlign: "center"}}>
        <Spin className="soma-loading" tip="Carregando...">{/* hidden={!loading} */}
        </Spin>
      </div>

      {discounts && <DiscountList />}

      {/* <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={discounts}
            loading={loading}
            rowExpandable
            expandedRowRender={(discount) => (
              <DiscountDetail discount={discount} />
            )}
          />
        </Col>
      </Row> */}
    </div>
  );
};

const DiscountDetail = ({ discount }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Detalhes do Desconto
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Desconto">
            {discount.discount}
          </Descriptions.Item>
          <Descriptions.Item label="Descrição">
            {discount.discount_description}
          </Descriptions.Item>
          <Descriptions.Item label="Local">
            {discount.company}
          </Descriptions.Item>
          <Descriptions.Item label="Marca">
            <img src={discount.brand} height={200} alt="" />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Endereço">
            {discount.address}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};

const DeleteDiscountButton = ({ discount }) => {
  const [loading, setLoading] = useState(false);

  const deleteDiscount = async () => {
    try {
      setLoading(true);
      await deleteDiscountService(discount.id);
      notifySuccess({
        title: "Tudo certo",
        message: "O desconto foi excluído",
      });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Esta operação não poderá ser
          desfeita
        </p>
      }
      onConfirm={deleteDiscount}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red" style={{ marginTop: 5 }}>
        <Icon type={loading ? "loading" : "close"} /> Excluir
      </Tag>
    </Popconfirm>
  );
};
