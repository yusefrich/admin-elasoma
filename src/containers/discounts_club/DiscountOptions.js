import React, { useState } from "react";
import DropdownMenu from "../../components/DropdownMenu";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { Icon, Modal } from "antd";
import EditDiscountForm from "./EditDiscountForm";
import {
  deleteDiscountService,
  editDiscountService,
} from "../../services/discountClub";

const { confirm } = Modal;

const EventOptions = ({ discount }) => {
  const [loading, setLoading] = useState(false);
  const [loadingDisablingEvent, setLoadingDisablingEvent] = useState(false);
  const [showEditEventDrawer, setShowEditDiscountDrawer] = useState(false);

  const deleteDiscount = async () => {
    try {
      setLoading(true);
      await deleteDiscountService(discount.id);
      notifySuccess({
        title: "Tudo certo",
        message: "O desconto foi excluído",
      });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  const setDisabledDiscount = async () => {
    try {
      setLoadingDisablingEvent(true);
      console.log(discount);
      await editDiscountService(discount.id, {
        ...discount,
        category: discount.categoryId,
        disabled: !discount.disabled,
      });
      notifySuccess({
        title: "Tudo certo",
        message: `Desconto marcado como ${
          !discount.disabled ? "'Inativo'" : "'Ativo'"
        }`,
      });
      setLoadingDisablingEvent(false);
    } catch (e) {
      console.log(e);
      setLoadingDisablingEvent(false);
      notifyError(e);
    }
  };

  const promptToDeleteDiscount = async () => {
    confirm({
      okText: "Quer excluir o desconto",
      title: "Você tem certeza de que deseja excluir este desconto?",
      content: "Esta operação não pode ser desfeita",
      onOk() {
        deleteDiscount();
      },
    });
  };

  const options = [
    {
      label: (
        <>
          <Icon type="edit" />
          Editar
        </>
      ),
      onSelect: () => setShowEditDiscountDrawer(true),
    },
    {
      label: loadingDisablingEvent ? (
        <Icon type="loading" />
      ) : (
        <>
          <Icon type="check" />
          {discount.disabled ? "Marcar como Ativo" : "Marcar como Inativo"}
        </>
      ),
      onSelect: setDisabledDiscount,
    },
    {
      label: loading ? (
        <Icon type="loading" />
      ) : (
        <>
          <Icon type="delete" />
          Excluir
        </>
      ),
      onSelect: promptToDeleteDiscount,
    },
  ];

  return (
    <>
      <DropdownMenu label="Opções" options={options} />
      <EditDiscountForm
        isOpen={showEditEventDrawer}
        onClose={() => setShowEditDiscountDrawer(false)}
        discount={discount}
      />
    </>
  );
};

export default EventOptions;
