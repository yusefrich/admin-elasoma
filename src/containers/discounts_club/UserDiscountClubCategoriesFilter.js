import React, { useEffect, useState } from "react";
import { Select, Radio } from "antd";
import { useSelector } from "react-redux";

const { Option } = Select;

export default ({ onSelect, initialValue = null, disabled = false }) => {
  const [selected, setSelected] = useState(initialValue);
  const { categories } = useSelector((store) => store.discountClub);

  useEffect(() => {
    console.log("those are all the categories for discount club");
    console.log(categories);
    if (initialValue) {
      setSelected(initialValue);
      onSelect(initialValue);
    }
  }, [initialValue]);

  const onSelectCategory = (categoryId) => {
    console.log("category selected")
    console.log(categoryId.target.value)
    
    setSelected(categoryId.target.value);
    onSelect(categoryId.target.value);
  };

  return (
    <>
      <div className="user_discount_radio">

        <Radio.Group  disabled={disabled} onChange={onSelectCategory} defaultValue={selected}>
          {categories.map((category) => (
            <Radio.Button key={category.id} value={category.id}>
                <img style={{ margin: 7}} src={category.icon} width="60px" height="60px" alt=""/>
                <span className="user_discount_radio_title">
                  {category.name}
                </span>
              </Radio.Button>
            ))}
        </Radio.Group>
      </div>

      {/* <Select
        value={selected}
        placeholder="Filtrar por Categoria"
        style={{ width: 220 }}
        onChange={onSelectCategory}
        disabled={disabled}
        showSearch
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        <Option value={null}>Selecionar Categoria</Option>
        {categories.map((category) => (
          <Option key={category.id} value={category.id}>
            {category.name}
          </Option>
        ))}
      </Select> */}
    </>
  );
};
