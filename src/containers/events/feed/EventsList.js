import React, { useEffect, useState } from "react";
import {
  Avatar,
  Button,
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Carousel,
  Row,
  Spin,
  Table,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../../helpers/notifications";
import Can from "../../../rbac/Can";
import { getUserFromStore } from "../../../helpers/store";
import { deleteNewsService } from "../../../services/news";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { firestoreTimeToMoment, formattedDate } from "../../../helpers/common";
import {
  createEventsByStateListenerService,
  createEventsListenerService,
  getEventsService,
  getEventsByStateService,
  deleteEventService,
} from "../../../services/events";
import UserStateFilter from "../../forms/UserStateFilter";
import EditEventForm from "../EditEventForm";
import ShowEvent from "../ShowEvent";
import ShowEventSlider from "../ShowEventSlider";
import ConfirmedPeopleFile from "../EventPeopleFile";

export default () => {
  const [events, setEvents] = useState(null);
  const [spotEvents, setSpotlightEvents] = useState(null);
  const [loading, isLoading] = useState(false);
  const [activeState, setActiveState] = useState(null);

  const user = getUserFromStore();

  useEffect(() => {
    getEvents();
  }, [activeState]);

  const getEvents = async (stateId) => {
    try {
      const getEvents = async (stateId) => {
        isLoading(true);
        if (stateId) {
          /* createEventsByStateListenerService(stateId, (events) => {
            console.log(events)
            setEvents(events);
            isLoading(false);
          }); */
          var ev = await getEventsByStateService(stateId);
          console.log("events return");
          console.log(ev);
          setEvents(ev);
          setSpotlightEvents(ev);
          isLoading(false);
        } else {
          /* createEventsListenerService((events) => {
            console.log("events returned");
            console.log(events)
            setEvents(events);
            setSpotlightEvents(events);
            isLoading(false);
          }); */
          var ev = await getEventsService();
          console.log("events return");
          console.log(ev);
          setEvents(ev);
          setSpotlightEvents(ev);
          isLoading(false);
        }
      };

      Can({
        perform: "events:see_all",
        yes: () => getEvents(activeState),
        no: () => getEvents(activeState),
      });
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Titulo do Evento",
      dataIndex: "title",
      key: "title",
      width: "20%",
    },
    {
      title: "Data",
      key: "date",
      width: "15%",
      render: (data) => (
        <p>
          {formattedDate(
            firestoreTimeToMoment(data.dateTime),
            "DD/MM/YYYY - HH:mm"
          )}
        </p>
      ),
    },
    {
      title: "Endereço",
      key: "address",
      width: "30%",
      render: (data) => (
        <>
          <p>{data.place}</p>
          <small>{data.address}</small>
        </>
      ),
    },
    {
      title: "Destaque",
      key: "spotlight",
      width: "15%",
      render: (data) =>
        data.spotlight && (
          <Tag color="geekblue">
            <Icon type="highlight" />
          </Tag>
        ),
    },
    {
      title: "Estado",
      dataIndex: "stateDetails.uf",
      key: "stateDetails.uf",
    },
    {
      title: "Ações",
      key: "actions",
      render: (event) => (
        <>
          <EditEventForm event={event} />
          <DeleteEventButton event={event} />
        </>
      ),
    },
  ];
  const EventsCards = () => (
    <>
      {events.map((row) => (
        <ShowEvent key={row.id} event={row} />
      ))}
    </>
  );

  return (
    <div>
      {spotEvents && (
        <Carousel autoplay>
          {spotEvents.map((row, index) => {
            if (row.spotlight) {
              return (
                <div>
                  <ShowEventSlider key={row.id} event={row} />
                </div>
              );
            } else {
              return null;
            }
          })}
        </Carousel>
      )}
      <Row gutter={24} style={{ marginBottom: 40, marginLeft: 20 }}>
        <Col span={4}>
          <UserStateFilter onSelect={(stateId) => setActiveState(stateId)} />
        </Col>
      </Row>
      <div hidden={!loading} style={{ textAlign: "center" }}>
        <Spin className="soma-loading" tip="Carregando...">
          {/* hidden={!loading} */}
        </Spin>
      </div>

      {events && <EventsCards />}
      {/* <Row gutter={24}>
        <Can
          perform="news:see_all"
          yes={() => <StateFilter onSelect={getEvents} />}
        />
      </Row>
      <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={events}
            loading={loading}
            rowExpandable
            expandedRowRender={(event) => <EventDetail event={event} />}
          />
        </Col>
      </Row> */}
    </div>
  );
};

const EventDetail = ({ event }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Detalhes do Evento
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Título">{event.title}</Descriptions.Item>
          <Descriptions.Item label="Descrição">
            {event.description}
          </Descriptions.Item>
          <Descriptions.Item label="Imagem de Destaque">
            <img src={event.image} height={200} alt="" />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Data">
            {formattedDate(
              firestoreTimeToMoment(event.dateTime),
              "DD/MM/YYYY - HH:mm"
            )}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Local">{event.place}</Descriptions.Item>
          <Descriptions.Item label="Endereço">
            {event.address}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item
            label={`${
              event.confirmed ? event.confirmed.length : 0
            } Confirmações`}
          >
            {event.confirmed && event.confirmed.length > 0 ? (
              <>
                <PDFDownloadLink
                  document={<ConfirmedPeopleFile event={event} />}
                  fileName={`${event.title}.pdf`}
                >
                  <div className="pull-right">
                    <Tag color="blue">Baixar Lista de Confirmados</Tag>
                  </div>
                </PDFDownloadLink>
                {event.confirmed.map((user) => (
                  <div key={user.id}>
                    <Avatar
                      size="large"
                      src={user.photoURL}
                      style={{ margin: "12px" }}
                    />
                    <small>{user.name}</small>
                  </div>
                ))}
              </>
            ) : (
              <Tag color="orange">Sem pessoas confirmadas até o momento</Tag>
            )}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};

const DeleteEventButton = ({ event }) => {
  const [loading, setLoading] = useState(false);

  const deleteEvent = async () => {
    try {
      setLoading(true);
      await deleteEventService(event.id);
      notifySuccess({ title: "Tudo certo", message: "O evento foi excluído" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Esta operação não poderá ser
          desfeita
        </p>
      }
      onConfirm={deleteEvent}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red" style={{ marginTop: 5 }}>
        <Icon type={loading ? "loading" : "close"} /> Excluir
      </Tag>
    </Popconfirm>
  );
};
