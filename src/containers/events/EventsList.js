import React, { useEffect, useState } from "react";
import {
  Avatar,
  Col,
  Descriptions,
  Divider,
  Icon,
  Row,
  Select,
  Table,
  Tag,
} from "antd";
import { notifyError } from "../../helpers/notifications";
import Can from "../../rbac/Can";
import { getUserFromStore } from "../../helpers/store";
import { PDFDownloadLink } from "@react-pdf/renderer";
import {
  firestoreTimeToMoment,
  formattedDate,
  sortArrayOfObjectsByKeyAndStringValue,
} from "../../helpers/common";
import {
  createEventsByStateListenerService,
  createEventsListenerService,
} from "../../services/events";
import StateFilter from "../forms/StateFilter";
import EventPeopleFile from "./EventPeopleFile";
import EventGallery from "./EventGallery";
import EventOptions from "./EventOptions";

export default () => {
  const [events, setEvents] = useState(null);
  const [loading, isLoading] = useState(false);
  const [statusFilter, setStatusFilter] = useState(null);
  const [selectedState, setSelectedState] = useState(null);

  const user = getUserFromStore();

  useEffect(() => {
    getEvents();
  }, [statusFilter, selectedState]);

  const getEvents = async (stateId) => {
    try {
      isLoading(true);
      createEventsByStateListenerService(
        { status: statusFilter, stateId: selectedState },
        (events) => {
          setEvents(events);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Titulo do Evento",
      dataIndex: "title",
      key: "title",
      width: "20%",
    },
    {
      title: "Data",
      key: "date",
      width: "15%",
      render: (data) => (
        <p>
          {formattedDate(
            firestoreTimeToMoment(data.dateTime),
            "DD/MM/YYYY - HH:mm"
          )}
        </p>
      ),
    },
    {
      title: "Endereço",
      key: "address",
      width: "30%",
      render: (data) => (
        <>
          <p>{data.place}</p>
          <small>{data.address}</small>
        </>
      ),
    },
    {
      title: "Destaque",
      key: "spotlight",
      width: "15%",
      render: (data) =>
        data.spotlight && (
          <Tag color="geekblue">
            <Icon type="highlight" />
          </Tag>
        ),
    },
    {
      title: "Estado",
      dataIndex: "stateDetails.uf",
      key: "stateDetails.uf",
    },
    {
      title: "Ações",
      key: "actions",
      render: (event) => <EventOptions event={event} />,
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col span={4}>
          <Can
            perform="news:see_all"
            yes={() => (
              <StateFilter onSelect={(state) => setSelectedState(state)} />
            )}
          />
        </Col>
        <Col span={4}>
          <Select
            value={statusFilter}
            placeholder="Filtrar por status"
            style={{ width: 220 }}
            onChange={(status) => setStatusFilter(status)}
            showSearch
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            <Select.Option value={null}>Selecionar Status</Select.Option>
            <Select.Option value={true}>Realizados</Select.Option>
            <Select.Option value={false}>Não realizados</Select.Option>
          </Select>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={events}
            loading={loading}
            rowExpandable
            expandedRowRender={(event) => <EventDetail event={event} />}
          />
        </Col>
      </Row>
    </div>
  );
};

const EventDetail = ({ event }) => {
  const UsersList = ({ users }) => {
    const orderedUsers = sortArrayOfObjectsByKeyAndStringValue(users, "name");
    return orderedUsers.map((user) => (
      <div key={user.id}>
        <Avatar size="large" src={user.photoURL} style={{ margin: "12px" }} />
        <small>{user.name}</small>
      </div>
    ));
  };

  return (
    <Row gutter={16}>
      <Col>
        <EventGallery event={event} />
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Detalhes do Evento
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Título">{event.title}</Descriptions.Item>
          <Descriptions.Item label="Descrição">
            {event.description}
          </Descriptions.Item>
          <Descriptions.Item label="Imagem de Destaque">
            <img src={event.image} height={200} alt="" />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Data">
            {formattedDate(
              firestoreTimeToMoment(event.dateTime),
              "DD/MM/YYYY - HH:mm"
            )}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Local">{event.place}</Descriptions.Item>
          <Descriptions.Item label="Endereço">
            {event.address}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item
            className="align-baseline"
            label={`${
              event.confirmed ? event.confirmed.length : 0
            } Confirmações`}
          >
            {event.confirmed && event.confirmed.length > 0 ? (
              <>
                <PDFDownloadLink
                  document={
                    <EventPeopleFile
                      documentTitle="Lista de Confirmações"
                      people={event.confirmed}
                      event={event}
                    />
                  }
                  fileName={`${event.title}.pdf`}
                >
                  <div className="pull-right">
                    <Tag color="blue">Baixar Lista de Confirmados</Tag>
                  </div>
                </PDFDownloadLink>
                <UsersList users={event.confirmed} />
              </>
            ) : (
              <Tag color="orange">Sem pessoas confirmadas até o momento</Tag>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            className="align-baseline"
            label={`${event.denied ? event.denied.length : 0} Recusas`}
          >
            {event.denied && event.denied.length > 0 ? (
              <>
                <PDFDownloadLink
                  document={
                    <EventPeopleFile
                      documentTitle="Lista de Recusas"
                      people={event.denied}
                      event={event}
                    />
                  }
                  fileName={`${event.title}.pdf`}
                >
                  <div className="pull-right">
                    <Tag color="blue">Baixar Lista de Recusas</Tag>
                  </div>
                </PDFDownloadLink>
                <UsersList users={event.denied} />
              </>
            ) : (
              <Tag color="orange">Sem recusas até o momento</Tag>
            )}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};
