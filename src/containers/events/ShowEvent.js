import React, { useEffect, useState } from "react";
import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tag,
  Upload,
} from "antd";
import locale from "antd/es/date-picker/locale/pt_BR";
import GoogleMapReact from "google-map-react";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import { getAddressService } from "../../services/commonn";
import mapPin from "../../assets/imgs/pin.svg";
import { createEventService, editEventService, updateEventService } from "../../services/events";
import { firestoreTimeToMoment, formattedDate } from "../../helpers/common";
import styled from "styled-components";

const { Option } = Select;
const { Search } = Input;

const MapPin = ({ text }) => (
  <div>
    <img src={mapPin} width={20} alt="" />
  </div>
);

const EventContainer = styled.div`
display: flex;
justify-content: between;
margin-bottom: 15px;
margin-left: 15px;
margin-right: 15px;
cursor: pointer;
`;
const EventTitleContainer = styled.div`
margin-left: 20px
`;
const EventTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
color: #9096B4;
`;
const EventSubTitle = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 175.9%;
color: #9096B4;
`;

const EventDetailHeader = styled.div`
width: 100%;
height: 300px;
background-position: center; 
background-repeat: no-repeat;
background-size: cover; 
position: relative;
margin-bottom: 15px;
`;

const EventDetailTitle = styled.h1`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 24px;
line-height: 110%;
color: #FFFFFF;
position: absolute;
bottom: 17px;
padding-left: 15px;
padding-right: 15px;
`;
const EventButton = styled.button`
background: #0F4DFF;
border-radius: 57.5875px;
color: #fff;
padding: 8px 45px;
border: none;
cursor: pointer;
position: relative;
top: -20px;

font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 14.3467px;
line-height: 175.9%;
`;
const EventButtonA = styled.a`
background: #0F4DFF;
border-radius: 57.5875px;
color: #fff !important;
padding: 8px 45px;
border: none;
cursor: pointer;
position: relative;
top: -20px;

font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 14.3467px;
line-height: 175.9%;
`;
const EventButtonDis = styled.button`
background: #9096B4;
border-radius: 57.5875px;
color: #fff;
padding: 8px 45px;
border: none;
cursor: pointer;
position: relative;
top: -20px;

font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 14.3467px;
line-height: 175.9%;
`;
const EventPlaceContainer = styled.div`
background-color: #151A2E;
box-shadow: 0px 0px 40px #000000;
border-radius: 5px;
`;


export default Form.create({ name: "edit_event_form" })(
  ({
    event,
    form: { getFieldDecorator, validateFieldsAndScroll, resetFields },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [loadingAddress, isLoadingAddress] = useState(false);
    const [eConfirmed, setConfirmed] = useState(event.confirmed);
    const [mapsCenter, setMapsCenter] = useState(event.mapCoordinates);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("50%");
    const [loadingConfirmButton, isLoadingConfirmButton] = useState(false);

    const [address, setAddress] = useState("");
    const [draggableMap, setDraggableMap] = useState(true);

    const [imageUrl, setImageUrl] = useState(event.image);
    const { states } = useSelector((state) => state.states);
    let timer = null;

    const user = getUserFromStore();

    useEffect(() => {
      return () => (timer = null);
    }, [timer]);

    const handleSearchAddress = async (query) => {
      isLoadingAddress(true);
      const address = await getAddressService(query);
      setMapsCenter(address.geometry.location);
      isLoadingAddress(false);
    };

    const handleSearchAddressInput = (e) => {
      clearTimeout(timer);
      setAddress(e.target.value);
      timer = setTimeout(() => {
        handleSearchAddress(address);
      }, 1000);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (imageUrl) values.image = imageUrl;

            await editEventService(event.id, values);
            notifySuccess({
              title: "Ok!",
              message: "O evento foi atualizado",
            });
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `events/${user.id}`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      if (windowWidth === 520)
        return (
          <Icon
            type="fullscreen"
            onClick={() => setWindowWidth("50%")}
            style={{ marginRight: 20 }}
          />
        );
      else
        return (
          <Icon
            type="fullscreen-exit"
            onClick={() => setWindowWidth(520)}
            style={{ marginRight: 20 }}
          />
        );
    };

    const onCancelForm = async () => {
      /* if (imageUrl) deleteImageFromUrlService(imageUrl); */
      isDrawerOpen(false);
      setImageUrl("");
    };

    const confirmPresence = async () => {
      
      try {
        const confirmed =
          event.confirmed && event.confirmed.length > 0 ? event.confirmed : [];
  
        const newConfirmed = confirmed.some(person => person.id === user.id)
          ? confirmed.filter(person => person.id !== user.id)
          : confirmed.concat([
              {
                id: user.id,
                name: user.name,
                business: user.business,
                city: user.city,
                cityName: user.cityName,
                cpf: user.cpf,
                phone: user.phone,
                photoURL: user.photoURL,
                state: user.state.id,
                stateData: {
                  id: user.state.id,
                  name: user.state.name,
                  uf: user.state.uf
                },
                stateUf: user.stateUf,
              },
            ]);
  
        const confirmedIds = newConfirmed.map(confirmed => confirmed.id);
        setConfirmed(newConfirmed);
        event.confirmed = newConfirmed;
        event.confirmedIds = confirmedIds;


        await updateEventService(event.id, {
          confirmed: newConfirmed,
          confirmedIds,
        });
        isDrawerOpen(true);

      } catch (e) {
        console.log("error");
        console.log(e);
      }
      isDrawerOpen(true);
    };
  
    const onMoveMap = (childKey, childProps, mouse) => {
     /*  setMapsCenter({ lat: mouse.lat, lng: mouse.lng }); */
      setDraggableMap(false);
    };
    return (
      <>
        <EventContainer onClick={() => isDrawerOpen(!drawerOpen)} >
          {/* <img style={{borderRadius: 90, marginTop: 20}} src={event.image} width="60px" height="60px" alt=""/> */}
          <EventTitleContainer>
            <EventTitle>{event.title}</EventTitle>
            <EventSubTitle>
              {formattedDate(
                firestoreTimeToMoment(event.dateTime),
                "DD/MM/YYYY - HH:mm"
              )}
            </EventSubTitle>
          </EventTitleContainer>
        </EventContainer>
        <Divider style={{ background: "#090C1D" }} />

        <Drawer
          title={[<IncreaseWindowSizeButton />]}
          width={windowWidth}
          onClose={onCancelForm}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80, background: "#090C1D" }}/* 151A2E */
          headerStyle={{ background: "#9096B4", borderBottom: "1px solid #090C1D", color: "#9096B4" }}
        >
          <EventDetailHeader style={{ backgroundImage: `url(${event.image})` }}>
            <EventDetailTitle>{event.title}</EventDetailTitle>
          </EventDetailHeader>
          <div style={{ textAlign: 'center' }}>
            {
              eConfirmed &&
              eConfirmed.some(person => person.id === user.id)
                ? <EventButtonDis onClick={confirmPresence}>Remover Presença</EventButtonDis>
                : <EventButton onClick={confirmPresence}>Confirmar Presença</EventButton>
            }
          </div>
          <EventSubTitle style={{ paddingLeft: 15, paddingRight: 15 }}>
            {formattedDate(
              firestoreTimeToMoment(event.dateTime),
              "DD/MM/YYYY - HH:mm"
            )}
          </EventSubTitle>
          <EventPlaceContainer>
            <EventTitle style={{ paddingLeft: 15, paddingRight: 15 }}>{event.place}</EventTitle>
            <EventSubTitle style={{ paddingRight: 15, paddingLeft: 15 }}> {event.address} </EventSubTitle>
            <div style={{ width: "100%", height: 300 }}>
              {getFieldDecorator("mapCoordinates", {
                initialValue: event.mapCoordinates,
                rules: [{ required: true, message: "Selecione o Local" }],
              })(
                <GoogleMapReact
                  draggable={draggableMap}
                  bootstrapURLKeys={{
                    key: "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM",
                  }}
                  center={{
                    lat: mapsCenter.lat,
                    lng: mapsCenter.lng,
                  }}
                  defaultCenter={{
                    lat: mapsCenter.lat,
                    lng: mapsCenter.lng,
                  }}
                  defaultZoom={15}
                >
                  <MapPin lat={mapsCenter.lat} lng={mapsCenter.lng} />
                </GoogleMapReact>
              )}
            </div>
            <EventButtonA style={{marginLeft: 20}} target="_blank" href={"https://www.google.com/maps/search/?api=1&query="+mapsCenter.lat+","+mapsCenter.lng}>Como chegar</EventButtonA>
            <EventSubTitle style={{ padding: 20, whiteSpace: "pre-line" }} dangerouslySetInnerHTML={{ __html: event.description }}></EventSubTitle>

          </EventPlaceContainer>
        </Drawer>
      </>
    );
  }
);
