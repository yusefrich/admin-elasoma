import React, { useState } from "react";
import styled from "styled-components";
import { Popconfirm, Icon } from "antd";
import { deleteEventImageGalleryService } from "../../services/events";
import { notifyError, notifySuccess } from "../../helpers/notifications";

const SingleImageGalleryContainer = styled.div`
  padding: 5px;
  margin: 5px;
  width: 150px;
  height: 150px;
  justify-content: center;
  align-items: center;
  display: flex;
  transition: all 0.5s;
  border: 1px solid transparent;

  &:hover {
    border: 1px solid #ccc;
  }
`;

const ImageContainer = styled.div`
  position: absolute;
`;

const OptionsContainer = styled.div`
  padding: 10px;
  position: relative;
  justify-content: center;
  align-items: center;
  display: flex;
  width: 100%;
  height: 100%;
  transform: all 0.5s;
  transition: all 0.5s;
  opacity: 0;
  &:hover {
    opacity: 1;
    background: #ffffffc7;
  }
`;

const SingleImageGallery = ({ event, image }) => {
  const [loading, isLoading] = useState(false);

  const deleteImageGallery = async () => {
    try {
      isLoading(true);
      await deleteEventImageGalleryService(event.id, image);
      notifySuccess({
        title: "Tudo certo",
        message: "A imagem foi excluída com sucesso",
      });
    } catch (e) {
      isLoading(false);
      notifyError({
        message:
          "Houve um erro ao excluir a imagem. Tente novamente mais tarde",
      });
    }
  };
  return (
    <SingleImageGalleryContainer>
      <ImageContainer>
        <img
          src={image.thumbnail}
          width={image.thumbnailWidth}
          height={image.thumbnailHeight}
        />
      </ImageContainer>
      <OptionsContainer>
        <Popconfirm
          title="Quer mesmo deletar esta imagem da galeria?"
          onConfirm={deleteImageGallery}
          okText="Sim"
          cancelText="Não"
        >
          <Icon type={loading ? "loading" : "delete"} />
        </Popconfirm>
      </OptionsContainer>
    </SingleImageGalleryContainer>
  );
};

export default SingleImageGallery;
