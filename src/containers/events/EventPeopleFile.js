import React from "react";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Image,
} from "@react-pdf/renderer";
import {
  firestoreTimeToMoment,
  formattedDate,
  sortArrayOfObjectsByKeyAndStringValue,
} from "../../helpers/common";

const styles = StyleSheet.create({
  page: {
    padding: 20,
  },
  headerContainer: { flexDirection: "row", alignItems: "center" },
  peopleContainer: { marginTop: 20 },
  cellHeaderContainer: {
    flexDirection: "row",
    borderBottom: 1,
    borderColor: "#ccc",
    backgroundColor: "#ccc",
  },
  cellContainer: { flexDirection: "row", borderBottom: 1, borderColor: "#ccc" },
  cell: { padding: 5, width: "35%", fontSize: 12 },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 2,
  },
  eventHeaderContainer: { marginTop: 10 },
  headerText: { fontSize: 14 },
  titleH1: { fontWeight: "bold" },
  sectionImage: {
    width: "30%",
  },
});

const ConfirmedPeopleFile = ({ event, documentTitle, people }) => {
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={styles.headerContainer}>
          <View style={styles.sectionImage}>
            <Image src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZIAAAA6CAYAAACaut/NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAByLSURBVHgB7V3dcxRVm396kkBAwInuu8oqMonohVZpwPd6HbJ3b4ECW+6Fa0nC3gv4B7wM/gEarN2LvVgJVu2NVmkQdu+WNO/VXqwkWKVlKUUGQVHWIhMSyBdJv79fz+mkMxlmzpnpmTkz07+qTmamz/R0n4/n+3mOIxEgBSwsLPR3dHTsdhyn3/O8JD/mf7zn62Thd3Auy/84z/85HDdwTOK4Ojs7O5EDJIYRksCWLVvS4XHA0c9zeJ8qbM8xwOfsZx5XJe7/GDFiVABHKgAJ1rZt244qYnVIijCKCDCB4zKO0Vu3brkSYwPUOJBRHMI4vFmMWVSBCVxvAtc91y79v2fPHpmfnz+OZz4hlcG7f//+PluZcCaTkZGRkZ6HDx9eUsKeMRKJxNjNmzePSRuhin7zlpeXB3799desWILUoCe//y5px5FkR4dM5EZ9QX79uRVJdWwSN3yuHIwYybPPPpvGv1M40lJHUHLGBHaXlpZO2zQojYIaBzLwo1IbJl6IHJjKaKszFSjWAmKRkfwcrxSn0UcZsRAvvfSSTE9PD2EsP5HKMYbnG5A2QpX9Zs182PGW58iCfOh4EghKk86M7Mu5Ti55yJOVh5IBgwnm/pSzLAO5/3YmdK6d0GlEwoVjDC95pKXOoKQNIjbY2dk5uWvXrrNPP/10StoQBeNwXOrDRAiayAb5u/j9K88888xRifEovEdNUSwEiCEFxz9LDCNU2W9WzIdMxpPEkiRDTIRILW/3BVJxHpLM+jQlQM9Kh7wnmijJSNgBINxfSoMYSDEEDAUErRqpsanQaEZegL2YcSNgJtdx9EuMQiQfe+yxQbEMlKqBwYjNny2PCPqtB+bnSk2l9cAU/zib/NfrTbKeTIsmHslIKPVjQYwrH4iNyICQTbaydqIY+YhYxMgDYGH14hhvZw3xEaD0+q5YhgcPHmC4HOvuy3ZEpMXVVSuhr2PbAS/9+EHvyx0HvbMwW6UyGd+LMQWN5HTQDq/PzVxwzvP15kWB3Uvex8usOj2Z6JQzfEGz1443vFO43mRwvcLfLMpISBg6OjrGbJdeeH+8z1aUjKmFKEZutRmJGiLG4FKsnayBQSjKj2UF0uk0gwhSYpkwYjsi1OLqppWkYcLK5aS3w/GFz0NgDoPeslziOTjP5cQf4QfpkB4cvbmLzlDwvdsXce41+RKf7+W56QtOH53tT/yzJ7IsJ8F0MmiWUtfb4CvawEiahYkE4H3iGGslqRhEiLbKZhoDaif0ndiswtcTVvkifvnlF6erq6ttTMFRIWItTtvfUA2+/RZ/FjcIDKlkOh9tRs0EDCJXLCKr2LmORSgunrxR0HSD0NhZ+AEmXAZSZkoqA21sDBvN4ho3CnJFVhFcXzGB3eo9b65S9S9J5gftcW+z5z8o309GqoM/DujXq+jfSfyfLhwDgjk+OMf+JyN4VaqTWLnoPsT9P37r1q3TEoN+rXSjo9yojXz//fcp+BXjAAkDqAi+tESnxSUZpPLzzz+fkxri5Zex8CfEhRYRRpaRWeEP/vCW5yzO+5rKbmgb70M7GeXn1Gj+d0JS84qZLC2BXItcdkL9gPejhb+7jpHgQQcrMKW4OEYxUc9nAakCyhyQBkE6asrMyJSgPp4FHzksTYoqmUiO4bkM062GeHEMaK5KJBKvVyBQUBI/hWtIGzATV0oTGYe5Papdw8AlqcNEcK8u5k5aYvjYtGmTs7y8TDpUrimaeMfQd2fLtKOgxXGoKSNxoVXsPOBNzjlyGHeO+5cb8HUMh9sE+SIwf6X5fsXx0whGGR585Yp8uNmTE5sPeOPTF519NIfBJ5KRhz4DeZNMpfB6/sOF34AAjEsRtaUYlNYxVCuJiwSNg1MBMdvfjLkOypw1bP5NcTHhz9y+fXtUIgYFCzCUUxWMgYfvDf300081XTRRwyCPxFtZWflHPCNtxaW06KnZ2dm+RmrJGEOu8etlzKQkhv+imSfR8nkkSovrBQO+Xq4tNX5YQ55nsqKU117IlQbqTZ8Y+js8IcmHD6V/9qLjqvc98HVcwendYCRDM18558AwHHy2EtzrQof0zWsmJa76SJSzVNdh6s7MzOytZYfw2jdv3uzFS1PJtulswcq/Y8pEqIGcRD/trwUTIaCGj3AMKDAUM42VgANC+1ErO+DBRKbQL+fLNGto6Keus5hjC2HElRg+DHxKFChIn8ggPtBoXzPfGZ3iO97wju844GUKo6r+9Qq044cyRgf84we9S8oXMnXyNemFY/0JMhG2W+miRJGP1MITufNF/SgeNZQkfmeYEVzb38hbsFY1EkjEGdEgwpx0ZCL1lLIoGWuojquAFNjTLL4ShgUyOsvQse5CAhqqZ5Y/mF0vFtclE+0EbSdtLhlSCBONBAelcq6fS2XaToHZPyENgIE2MgRG8hcdCVxaXCMx1UYgaPXxNeaO0yitpEjG+hQYxL7Aad5zwHOgddyVvPbsTV9wHpn2EWgvdLoXO//EEc9ZXhL2TUp9BDVGBsIXfF00AA6cqTdhoGQsBpoJCLOtuS8bsH379lMmTESVKdlf71Ix+L3JLVu29PH3db/DaC7Lk7GqAoiNK+V9IMlGVAIgU6TPQ0cbeeGFF5rKBFlLVKCNrL6XBmkliSU/JyS8znpWHspg8KZb/ByRIWoZzoqfK7KKQMNI/ilf3DWI3ErnP+9l7giO6zz/0lse6L9vtUqFLkEm9W6YkWiZIWoddfAoqHo1ugysKUwqNGmZFAhE2zPo/0FpEH744QcPTHrIhJkAf27hhEUd4hE4WesKOot1fhfE8GOJ4YPayL1791LoEx2hOgct7nLwhkEN6HNX9GhUOso14TzM30/oI89L+JW8fTBHJHfBGYXzfH/uv5xhxSRSNEt9/CPmwLIMov06/9jEhLD2FudPCkcvy6X808v+qeyGG/BkOsxIdEJvJ6SxOKPTqFnyLxhqbdB8Akyk4dL922+/7eG+T4r+XKCE15L1nUg8uru7XQ3/UbqeCYoBQdSJwAQxrIl/rRlx584dgeM8rUM/0LejhVYBMCAKFjo0ysHvHJeIsHMLNI4O39Sa9S8OP0fg96AW8Ri0ibDfxM81WZZ0YkXevPufjrz/mgw7nTJABtOt2jEPP5Gvvk54UGkuU1tJPAazmYS0Gk/GEp1y2mckzSIxcvBY56ncIY1neGWhtBEtSZWECvZXK8KaWVIbZpMp3M8R0dQQGU5sayHDaoF+8DQ0tLomKOqaZ3DfI3E17TUYlENhefgNmujg4CDNnWQkZdcFtcWo1sR3n/vmqHH4PpiR7uQuOhT0fN/JrQX5qLNDxpndHiQl/v/nvoYyAg2Fa9g3Z/1dl+SghezdvCy+GSs74mfBj6ks+FWHPBnPidfkoyA7HtcYoCms0+SGK93DICpAIieDGJIWgEr81GpLv5RNC951Xe7dMTk3N/cBkxA1vuLQVwLXWkZaDCQeIyMjZ8BQylVjTpNw1Nq/GEpAfLNMUxLDOHFUQZWKH9TURooy4EDIkrxWUo6R99R6TWxaEFlagfM/H1KVkm3SD5+Iq0J//8OZkWNBoiKZ0c4D3vicyJGgdLyqz7VhvoY+Xz3nayS6RIqdbFMNoWaGph3W10Ya5ZcqhWvXrgmc78MGYcF1KRFRb5B4SL6CajmThlOPwIPAPCPlTdVurI2swUQbwZz/9FEnQ1qJDiIr5hiYsLYfWCuy+7ebca+dcixwskNTcdd9Kbk2R5ikOMOIs05RTESF+b7hnSos1PgH/Nb2P3mD4VDjsI9EV1KKa/ZUiV27dh3S9eNQGxF74Rk4a5OtKoQYEI+aV4HVJIgcN11i1/IwLM7olgrdpWABp3tOMyAlkjwjVue9NScZmrASjnzpR1nBjLVq8lJO9vz95XNIYAI7An9HlrknyQNeevp3+G3wXTjYfd/Nv+X9KIdZrJGFGuGQ/4ifq9IqY4kOOctNsJjUSFNYmJHo+hXoOPwyLh1eFbTCk23VRgK88847NNGNiJ4QEpQMaTkorYR94JZpWtMERZMERPhRyiVTtg1mZmYY4aajMev4w2RxcZFai+66jUK4cEI7GxKplcfWwn8D7YKl5cOaBQsyQlM5BNNX/6t/9B32e+k457nlTcIU9/B9TVHrWVryI2LToc/9DbDCjOSy6OOQKt9+NGYo5sBkfFWnHaRGVywGCSh8AySgWotGFYZsVWjlEWDsa7YniGa12sL8h7YG820wJmnRSBnQFexUfTNX9OqsMSG5KgGrSPjvOnz8jbD0yTgz2wuc7p6vrVxwht28ppINEhHpVN/cDac6fNLcwwQayPul7mGVkeDBh8Xk5vOVe0fUboXcgvV4vCdFeWDichB165lZnyj21FNPUfrSFUJadn7oEo9a+RmpjSwtLRVKi8V+32ott96AQKytjRgyYJMExapCgZ3H/YTD8G9lE11rFXq9Rf9foF2k5pPF/WdBfonv+4CvJYjugm8lQwZDU9mWhzLhrBccp8BkPlhlJFgIOcNEs3X3gIOO13EsEk8xlmEyFy6amMGsARK8dl/Mzs5aH8Z88OBBLkZXs3mylefCk08+SaZazmdUk1BgzCsnkUiUJUi2a7n1BCPc7t+/n9LZBdaUAVOwwHdc0XAZVLsRGrWH/nxIbq/amGpfeE8Rr9MvY3IGrG2CTvd5tWGVqs01HJi7vslrLmO+78ORLwLNJYzbFx1viwfzqcB/sgxtpUP8DbDWhf8uLy9nVNhgtTa7tIQkIxrwWFpc8p1K1ekqDmZeXsUg5lRYb1uAtarYHxqYaIYaVTRvgTnwPrOazkoykpYc76+//lr6+vpGYSNnf5QMBY5yrxKDPUeK5j+0K4J8G50w/EoY8ObNm725ublPySjKNA2EC1cqhJsPyc0G74OaWXytzFUZdTDMV2bvyeEEC8U6nBTyCj4e8DUXJzRv85pLLrjW0qKkujZJ7naeSY2Gf39d8S6GA+Kha5mnEajeQcn0sZAWM16oybRiEptB1v0NaRJ0d3ezGq4uc2i5MQ1DM7tZx5ehjTgB0Rwm2f9SIQM2DEaJrPoBGIXz4ddyFgxiCsfdHQfXl2Ha/Dcw6Xmhe/Lya1JlyO/HBB5haXlqGuk8E+nFdSZVYuP1oOJvGBuqQN68eXOU1UCl/tjAZLZt2zZFBrNr166zdOy3iFlEi5AGu0s2A2DaIgGd1mhKAtrSjEQ3u5nmlKgEJZhndPKSyODiuloKJtsPV8qADXKMiEhMnszxmHN809NgcF28/jBspmLWetcWCPFwpJNpOJ3iZ7ircOGJexecoSCT/dtv/RL04X13nISXDwUOo2g5YVbbReftdcz2oKgV+lliQ5U/GQczmSRjadacBN3qAHhW681aAR48eKB9v2j3nLQwDCLZIgkFNsiBcNvJhFwK9dBGApiUTZEItJIiYbs+Ch3sgSOdTCNXYvOqZW7Z66yr9kskC/0nj6xLz0m3tLS0vwoHfE3ABUPGgpcMP2bE2PE4BLmxwEKhtNs0jK/W2LFjh6e5bqrOIdBNQLRtHTcSutsPK1RVAcAwRL5qk+crL4Cod/j+i/B6zNLBHspWX+dkz99n6FxowyrZ4keErddkPRkt3AM+ISXADmTZcnREr5qIVhELJYUNqxDkOOM+hhX47rvvaBun9O+WaVpVDoHJniNxyO8aICA7utqI6IXwlsSePXs80FCtSgLVFjil072/XyYZvcXdDsEFTjCKi+eCfBLHY4StHPez0hUz+ff/E2dlGZ/zHBQpmK/O0rfCiLAulU+CZqPMKfHLrhQgoXNzAUOZnZ3tVf4TV+xDhhpKrJ3YDU1fSiugHnuV6JZDiRMQFUzKoZABRxFZxyKn3d3dWdGjm1XXZAuSC+9ddE7c+8o5w6gt1tJaXPR90KlQU26AdZTayFynJDGZwnORc8v3u61WC77gHA5ySgp/U4uRBGA4Kv0n3KGP29miow+DsfiOcckvnOBoCDg5oJ3Qj7KXdlAboetLaHSlZRNwEyXNLXi9ZvL9VAOD7OaK7OKc3/Pz8ykpv7Xrug2Y2h3M/hfN4owR17kjXdRl6O/19PREuv633qcJeoNFyd8AK7P2ft15dNTjogkjRhIGmQojvMBYTnIPZxwJEJN9IBRHQsxlXNYzmHowmSTu4RL3XRY7oeuUTkmTYHGRRXuc3TptmTckbQL6joByNa18wmYq+IBROToboxXbgKldobL/tbUR0LZPJSIowYIM3dVo3gOTZ9WBGGEwImulSyZo7pKAFnsy3pGQr2Rtw6rTskan78Ikdkz3+hUzkmKgg76AuewLGAxOcwevk3ViMkkM2idKjbUKumG9rMelwgebBVqh2c0U1lwtVB6Bjm8xzYRC0USgjWjU7YoTEEO4d+9eo7QRHwsLC9TItUvMR62V3Pvc8Z7dkveZ8Ji+6LwWrq3Vz50SO6QPfpAB/H++VDRXIYw2tqoUobBDt/AcHIYpOKJSyjTyaiKR2K1eB4RJKw28CNLT09O0+dnmZNQKwaTUNDw87GeWisUgsxsZGeF96oY1N02iZbUw2OjIgeAzKCrzuBxCe46URJyAuAYGJoDOpHU1fTDgv0jEuH37NisfnNeofEAkt27dOjg1NTUsEYKaiTyCBoWy47NiiLowklLIUucrcuOMXIDTiTkkb2Lw6fQhYzFiKvjeIBbzOZske0io3DZXqy2fHxZEVyzGhQsXuOjSuu3bLZfBYAdFSqDDIBxlBQdNO79JKfOWByO1AK2EP0aoglHfgK+1UiG21H3wH8Npy90Lf/s9MMDhPIm0Gw1nJI9CLg9XlBajstqPqygX3QFO2ybVszgmnKskpmVNQaqYnCsWg3s5iOb+KmL5s9QCFGJ27tyZg6Z9vkyElm8XByPJlLqe7pawtPHDzBw72WVdLbK0Tnv03SFVWj5ywGQW/EbZthxjalHSBOsmUh9JLUFJFscQBvh9k+9Rqhf7oLXASXhAhMRmwFZPhveGTlu0uyptCBbvY2UGjaaUQEs2mJ2d1Sl7XhMbf7NCNzAhhKTaJqNmh+Z9kNucKjcnbEDTMJIAYCa0GWrHxWuGpdYbo5rtOKGt3VWQ0jHMWoOi5x8hMdV97paCQcROUkmgRUGCAgbB8yWFo6gjjpoZBoEJtuL1UnPCFnRicmqZiRjKeO3aNbEB3IRLw+bsw8YwWiY5wbyl43BjVd0Te/bsOW9L34dB6Vg0C83FppbVPIJ0iTaBBOo+wi5ObWSwTNnzeD/2EAJtxPOiDAqtK8rNCSuQAEG+pHPMzc1FGtdcDehnkAoiCyyD7mJPs+9tS7AEI3RAsI5rMuq2r/VEIoA+uKxRCLWoBGoiWUNLjPdjl5bQRgJYr5XQtBWUby95sKaPZXkNuhniWbEQmBgjum3R96dsSrDkAmX9NSkd0roOaN/WjISAZKzDUIuWTVFlzzNlvhuH/IYALd4xKM5oM/w5YWu1DiJhkGn8uoqAajhUh+rmLWTFQqjFPqbZ3E+wfPHFFyMPR6wEP/74o9PR0fE/or+3SkzcxGivkqOFyWisHgAN8O+lNOIERAUKvejrpGZxRuvB5zBJWq03EiY727GYmA3Z4qpDdTOprU3og5SuXYJA8iauhjITlWDH3z9r4HuKiZuCwUZHTrhEhkFpDzdm2Hl89tln7LNDJvO0gYcOgqRVK9EJKYd2W908gOPT09PnoRGMs6JlI8BwWHSolvNMVe+0NgGOix6+BjpgtUxELDENZiK7du069vzzz3v1HAMSszNnzvRs3br1I0e/Yq3vYI6J2xpCCYrlxnw1QVF3zxGs5UizoJsZBsUZua7O4l/kmewGSGuuKe2k1XqjE9LiKAjzR5rtqW5/AY1gYM+ePZP1jiQiMcvlcod11VUsLFcsByPQMAbv4pm0fCBqU69+jMER9Mck976oNehYv3v3bgpS8heiqQkSWByTN2/e1A7VbgcorY6EwJXSEVx+iYy+vr7hO3fuHCoXxk6hCX6UrySGdtKmgq8xN0rYUSWGzkOwYJh/OVOxVtJqI5BQHejqfkGVar8OyfgkSwjUw9TF+HmaVGZnZwdhivtE93vNUCKCEWhgeNwzecrga/3wUVziYqnVGHCC87rPPfccJbvjGPMrYsBEgCmYFv5BYhSDTklxOljf/O2331iiP05ANIChNtJQ/53hvu5E5MUco0CQkGgsNWKSf4h/10nMIDU5tN2T8EQVWcBrQevxGQi49X6GIGOxUAXVdbJPRrEpTT3ArH3TjH2VIcv+uB4wFI5BNVmwSloWXodmLFx3CNLadZyixmoyeSnlDcUmreIwSFB8XWnf6VKN4gTENZiUihdL/HehIAwdRF5iPgqsOm5hd/8Ck/awVIYcs5bpb+EmOlDJs6wps2nTJkabeOgk/zUkhXVfWlhYEHBXPyKFhQy5QRJ9AF1dXTzXr4o1UuVLiyHwLHubrUAgTEgZMQipDYNhztDWXIaXKiLl92l3d7d3//59lunY8B2Mk9/38/PzlH6ZYMitX6l10GfG2PtKJB9f2gYTbzqTlqoQm5HyY8BnHKhGUAmZX85KlaCNH3PdJHBjHVQtql5aGjSaj3GLCLEUSvC8JHo0YxTPUinNixTw/TpYv2c1fSVTO3bseKIeZm1drBZthKnkGJhAv66tvgAMs+OiGCTTUGWSJ3C9LD6/wc3vwSCmC0Nx8T6JxcQyINyJK4n2u9G+H9+ntF2x+sY9T5qxyiwmdQbMhC+plptWOk4p/8mgqi7s4rgKJjGJcz9xDAq/g75Pqb7nFsqvKCZSjdrctEyk3iARgCZ/DnPdVNsrRBwVpxCUihc9JsK5ak1wAuuxqdwyHUaStG2LjFVGQlv9008/PQBt4FKFzCQMLox0OLKqWLVLcOB17yMqYzDOjbWkSUFmAjNVTpkOqwn1TatjQz8HcJxqLr8BU7jeSTjX2z7xUBfQ2jnhy+1VUhJxjs466JSQCRpaVbKH5k4wwstgJq6UZ4T+c6L9OVvKpqyjMJyQsC8O0L8gzYlxmGesVbt1wcKUmFB9TTIOLMY4icU7EDMRM+gmKJZAvOeIgmE5FFuDE3SCMAJYVTZlg6gaMBNKOqKfLNNosPTEMJkINzGRFkCTjAP7/czMzMy+dtuwKgowuEGZHCtlBi40WGuk6kbCpFS8rcEJhvu6W1VivqjNg0RM7f1xTEnFNjOUKdiIj9Cc1SpMJEBoHPZZNg68jzEc+1ux3+sJOE0rLWjpKSGj7dEi2oiPrVu3mswHa7SSkvuRgEiMwAm/z0KGwvtg7PVpaCF9t2/fHpUWBqV9mI36OA6SJ+CNGodVBsLInVgarh50ukOSpjbnmnwvDvldg+HGVbla7MceFTgf4HgnI9ERzvx8GRuKOSbKNaATngwlIGSYwF+KWY2YqBD8JgnZSTIQOqbbSRrmOKjQS5q8ztWJuftMm6ZDiRlIrWBiG/fbN0iqts4yEWgjGgUtCUr7o7YHJ4SCMHSQtqGYo9Ge7SRk+DeSBLZv357GA7+ucj3CGc9RhAKFJywjmJgfcRn2w/NZm3d3qRNU/oIbhDsyaQ199KpUPw6r/a7K73Of8dF2YRzMq5mens7imcfKtY2yGCin9M6dOy8nEokR/Pbucu3RJodxj1qqDoS00o0s2y5Z+RXYZ5PqKIlmCJUO1WPTYY5M3SANzkoDEVn85zPPPNOPhUA7JRMJnwtqAwX/H5VpGuwXwsVBdR3M6Qb/47iBDpqIGYc+yODVHvWvMoQbffiK5HN8ksX6P+h7VQE6h/f8/w20vYnY7xEjRgxd/BXGjBJD6hvnxwAAAABJRU5ErkJggg==" />
          </View>
          <View style={styles.section}>
            <Text style={styles.titleH1}>{documentTitle}</Text>
            <EventHeader event={event} />
          </View>
        </View>
        <View style={styles.peopleContainer}>
          <View style={styles.cellHeaderContainer}>
            <View style={styles.cell}>
              <Text>Nome</Text>
            </View>
            <View style={styles.cell}>
              <Text>Empresa</Text>
            </View>
            <View style={styles.cell}>
              <Text>CPF</Text>
            </View>
          </View>
          {sortArrayOfObjectsByKeyAndStringValue(people, "name").map(
            (person) => (
              <UserCell person={person} />
            )
          )}
        </View>
      </Page>
    </Document>
  );
};

const EventHeader = ({ event }) => {
  const date = formattedDate(
    firestoreTimeToMoment(event.dateTime),
    "DD/MM/YYYY - HH:mm"
  );

  return (
    <View>
      <View style={styles.eventHeaderContainer}>
        <Text style={styles.headerText}>Evento: {event.title}</Text>
        <Text style={styles.headerText}>Data: {date}</Text>
      </View>
    </View>
  );
};

const UserCell = ({ person }) => (
  <View style={styles.cellContainer}>
    <View style={styles.cell}>
      <Text>{person.name}</Text>
    </View>
    <View style={styles.cell}>
      <Text>{person.business}</Text>
    </View>
    <View style={styles.cell}>
      <Text>{person.cpf}</Text>
    </View>
  </View>
);

export default ConfirmedPeopleFile;
