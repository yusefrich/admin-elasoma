import React, { useState } from "react";
import { Modal, Form, Input, Button } from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { sendPasswordResetEmail } from "../../services/auth";

export default Form.create({ name: "forgetPasswordForm" })(
  ({ visible, handleCancel, form }) => {
    const [loading, isLoading] = useState(false);
    const { getFieldDecorator } = form;

    const handleOk = (e) => {
      e.preventDefault();
      form.validateFieldsAndScroll(async (err, value) => {
        if (!err) {
          try {
            isLoading(true);
            await sendPasswordResetEmail(value);
            notifySuccess({
              title: "Tudo certo",
              message: `Um email para redefinição de senha foi enviado para ${value.email}`,
            });
            handleCancel();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError({ title: "Houve um erro", code: e.code });
          }
        }
      });
    };

    return (
      <Form onSubmit={handleOk}>
        <Modal
          title="Esqueceu a senha?"
          visible={visible}
          confirmLoading={loading}
          footer={[
            <Button key="back" onClick={handleCancel}>
              Cancelar
            </Button>,
            <Button
              key="submit"
              htmlType="submit"
              type="primary"
              loading={loading}
              onClick={handleOk}
            >
              Enviar
            </Button>,
          ]}
        >
          <Form.Item label="E-mail">
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "O email deve ser válido",
                },
                {
                  required: true,
                  message: "Insira o email",
                },
              ],
            })(<Input />)}
          </Form.Item>
        </Modal>
      </Form>
    );
  }
);
