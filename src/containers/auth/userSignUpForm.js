import React, { useState } from "react";
import { Button, Checkbox, Form, Icon, Input } from "antd";
import { notifyError } from "../../helpers/notifications";
import { signInUserWithEmailPassword } from "../../services/auth";
import ModalForgetPassword from "./modalForgetPassword";
import { store } from "../../store";
import { push } from "connected-react-router";
import RegisterForm from "../../containers/users/RegisterForm";


export default Form.create({ name: "auth_login" })(
  ({ form: { getFieldDecorator, validateFields } }) => {
    const [loading, isLoading] = useState(false);
    const [modalForgetPassword, isOpenmodalForgetPassword] = useState(false);

    /* const handleSubmit = (e) => {
      e.preventDefault();
      validateFields(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await signInUserWithEmailPassword(values.email, values.password);
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    }; */

    return (
      <div style={{background: "#090C1D"}} >
        <RegisterForm/>
      </div>

    );
  }
);
