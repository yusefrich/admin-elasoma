import { getUserFromStore } from "../../helpers/store";
import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { getCitiesService } from "../../services/state";
import { updateUserService } from "../../services/user";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { Avatar, Col, Descriptions, Divider, Row } from "antd";
import UpdateableItem from "../../components/UpdateableItem";
import { userCan } from "../../rbac/Can";
import UpdateableEmail from "../App/UpdateableEmail";
import SetValidityDate from "./SetValidityDate";

export default ({ user }) => {
  const loggedUser = getUserFromStore();
  const { states } = useSelector((store) => store.states);
  const [loadingField, setLoadingField] = useState(null);
  const [cities, setCities] = useState([]);

  useEffect(() => {
    getCities();
  }, [user]);

  const getCities = async () => {
    if (user.state) {
      setCities(await getCitiesService(user.state));
    }
  };

  const updateUserData = async (value) => {
    try {
      const field = Object.keys(value)[0];
      setLoadingField(field);

      if (field === "state" && value.state !== user.state) {
        delete user.city;
        delete user.cityName;
      }

      await updateUserService(user.id, { ...user, ...value });
      notifySuccess({
        title: "Tudo certo",
        message: `Os dados de ${user.name} foram atualizados`,
      });
      setLoadingField(null);
    } catch (e) {
      setLoadingField(true);
      notifyError("Não foi possível estar este campo");
    }
  };

  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          <Avatar
            size="large"
            src={user.photoURL}
            style={{ marginRight: "16px" }}
          />
          {user.name}
        </Divider>

        <Descriptions
          layout="vertical"
          bordered
          size="small"
          // column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item label="Nome">
            <UpdateableItem
              loading={loadingField === "name"}
              value={user.name || "-"}
              field="name"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          {userCan({ role: loggedUser.role, perform: "user:show_role" }) && (
            <Descriptions.Item label="Nível">
              <UpdateableItem
                type="select"
                options={[
                  { id: "USER", text: "Usuário" },
                  { id: "ADMIN", text: "Administrador" },
                  { id: "SUPER_ADMIN", text: "Super Administrador" },
                ]}
                selectIdKey={"id"}
                selectTextKey={"text"}
                extraClass="full-width"
                value={user.role}
                field={"role"}
                onChange={updateUserData}
              />
            </Descriptions.Item>
          )}
          {userCan({
            role: loggedUser.role,
            perform: "users:set_validity",
          }) && (
            <Descriptions.Item label="Data de Vigência">
              <SetValidityDate user={user} />
            </Descriptions.Item>
          )}
          {user.email && (
            <Descriptions.Item label="Email">
              <UpdateableEmail user={user} />
            </Descriptions.Item>
          )}
          <Descriptions.Item label="UF">
            <UpdateableItem
              type="select"
              options={states}
              selectIdKey={"id"}
              selectTextKey={"name"}
              extraClass="full-width"
              value={user.state}
              field={"state"}
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Cidade">
            <UpdateableItem
              type="select"
              options={cities}
              selectIdKey={"id"}
              selectTextKey={"name"}
              extraClass="full-width"
              value={user.city}
              field={"city"}
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="WhatsApp">
            <UpdateableItem
              loading={loadingField === "phone"}
              value={user.phone || "-"}
              field="phone"
              onChange={updateUserData}
            />

            <UpdateableItem
              textFalse="Dado privado"
              textTrue="Dado público"
              extraClass={`full-width
                ${
                  user.public_phone === true
                    ? "ant-tag-green full-width"
                    : "ant-tag-red full-width"
                }
              `}
              loading={loadingField === "public_phone"}
              value={user.public_phone || false}
              type="toggle"
              field="public_phone"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Instagram">
            <UpdateableItem
              loading={loadingField === "instagram"}
              value={user.instagram || "-"}
              field="instagram"
              onChange={updateUserData}
            />
            <UpdateableItem
              textFalse="Dado privado"
              textTrue="Dado público"
              extraClass={`full-width
                ${
                  user.public_instagram === true
                    ? "ant-tag-green full-width"
                    : "ant-tag-red full-width"
                }
              `}
              loading={loadingField === "public_instagram"}
              value={user.public_instagram || false}
              type="toggle"
              field="public_instagram"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Telefone Celular">
            <UpdateableItem
              loading={loadingField === "cellphone"}
              value={user.cellphone || "-"}
              field="cellphone"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="CPF">
            <UpdateableItem
              loading={loadingField === "cpf"}
              value={user.cpf || "-"}
              field="cpf"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Youtube">
            <UpdateableItem
              loading={loadingField === "youtube_video"}
              value={user.youtube_video || "-"}
              field="youtube_video"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Sobre">
            <UpdateableItem
              value={user.aboutMe || "-"}
              field="aboutMe"
              onChange={updateUserData}
            />
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Dados Empresariais
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Nome da Empresa">
            <UpdateableItem
              value={user.business || "-"}
              field="business"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="CNPJ">
            <UpdateableItem
              value={user.cnpj || "-"}
              field="cnpj"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Qtd. Funcionários">
            <UpdateableItem
              value={user.employees || "-"}
              field="employees"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Média de Faturamento">
            <UpdateableItem
              value={user.billing_average || "-"}
              field="billing_average"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Pontos fontes como empresário">
            <UpdateableItem
              value={user.strongPoints || "-"}
              field="strongPoints"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Dúvidas como empreendedor">
            <UpdateableItem
              extraClass="full-width"
              value={user.entrepreneurDoubts || "-"}
              field="entrepreneurDoubts"
              onChange={updateUserData}
            />
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};
