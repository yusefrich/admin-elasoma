import React, { useEffect, useState } from "react";
import {
  Avatar,
  Col,
  Descriptions,
  Divider,
  Icon,
  Input,
  Popover,
  Row,
  Spin,
  Table,
  Tag,
} from "antd";

import {
  showUsersListenerService,
  updateUserService,
} from "../../../services/user";
import { notifyError, notifySuccess } from "../../../helpers/notifications";
import rolesEnum from "../../../models/rolesEnum";
import Can, { userCan } from "../../../rbac/Can";
import { getUserFromStore } from "../../../helpers/store";
import StateFilter from "../../forms/StateFilter";
import UserOptions from "../UserOptions";
import UpdateableItem from "../../../components/UpdateableItem";
import { useSelector } from "react-redux";
import { getCitiesService } from "../../../services/state";
import UpdateableEmail from "../../App/UpdateableEmail";
import ShowUsers from "../ShowUsers"
import UserStateFilter from "../../forms/UserStateFilter";

export default () => {
  const [users, setUsers] = useState(null);
  const [cpf, setCpf] = useState("");
  const [cnpj, setCnpj] = useState("");
  const [activeState, setActiveState] = useState(null);
  const [role, setRole] = useState(null);
  const [isSuspended, setIsSuspended] = useState(null);
  const [state, setState] = useState(null);

  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getUsers();
  }, [activeState, role, cpf, cnpj, isSuspended]);

  const getUsers = async () => {
    try {
      isLoading(true);
      showUsersListenerService(
        { filters: { stateId: activeState, role, cpf, cnpj, isSuspended } },
        (users) => {
          setUsers(users);
          console.log("all users data");
          console.log(users);
          console.log(activeState);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const UsersList = () => (
    <>
      {users.map((row) => {
        if(!row.isSuspended){

          return (<ShowUsers key={row.id} affiliated={row} />)
        }else {
          return null;
        }
        })}
    </>
  );


  return (
    <div>
      <Row gutter={24}>

        <Col span={4}>
              <UserStateFilter onSelect={(stateId) => setActiveState(stateId)} />
        </Col>
      </Row>
      
      <Row style={{ marginTop: 24 }} gutter={24}>
        <Col span={24}>
        <div hidden={!loading} style={{textAlign: "center"}}>
        <Spin className="soma-loading" tip="Carregando...">{/* hidden={!loading} */}
        </Spin>
      </div>

          {users && <UsersList/>}
        </Col>
      </Row>
    </div>
  );
};

const UserDetails = ({ user }) => {
  const loggedUser = getUserFromStore();
  const { states } = useSelector((store) => store.states);
  const [loadingField, setLoadingField] = useState(null);
  const [cities, setCities] = useState([]);

  useEffect(() => {
    getCities();
  }, [user]);

  const getCities = async () => {
    if (user.state) {
      setCities(await getCitiesService(user.state));
    }
  };

  const updateUserData = async (value) => {
    try {
      const field = Object.keys(value)[0];
      setLoadingField(field);

      if (field === "state" && value.state !== user.state) {
        delete user.city;
        delete user.cityName;
      }

      await updateUserService(user.id, { ...user, ...value });
      notifySuccess({
        title: "Tudo certo",
        message: `Os dados de ${user.name} foram atualizados`,
      });
      setLoadingField(null);
    } catch (e) {
      setLoadingField(true);
      notifyError("Não foi possível estar este campo");
    }
  };

  return (
    <Row gutter={16}>
      
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          <Avatar
            size="large"
            src={user.photoURL}
            style={{ marginRight: "16px" }}
          />
          {user.name}
        </Divider>

        <Descriptions
          layout="vertical"
          bordered
          size="small"
          // column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item label="Nome">
            <UpdateableItem
              loading={loadingField === "name"}
              value={user.name || "-"}
              field="name"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          {userCan({ role: loggedUser.role, perform: "user:show_role" }) && (
            <Descriptions.Item label="Nível">
              <UpdateableItem
                type="select"
                options={[
                  { id: "USER", text: "Usuário" },
                  { id: "ADMIN", text: "Administrador" },
                  { id: "SUPER_ADMIN", text: "Super Administrador" },
                ]}
                selectIdKey={"id"}
                selectTextKey={"text"}
                extraClass="full-width"
                value={user.role}
                field={"role"}
                onChange={updateUserData}
              />
            </Descriptions.Item>
          )}
          {user.email && (
            <Descriptions.Item label="Email">
              <UpdateableEmail user={user} />
            </Descriptions.Item>
          )}
          <Descriptions.Item label="UF">
            <UpdateableItem
              type="select"
              options={states}
              selectIdKey={"id"}
              selectTextKey={"name"}
              extraClass="full-width"
              value={user.state}
              field={"state"}
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Cidade">
            <UpdateableItem
              type="select"
              options={cities}
              selectIdKey={"id"}
              selectTextKey={"name"}
              extraClass="full-width"
              value={user.city}
              field={"city"}
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Telefone">
            <UpdateableItem
              loading={loadingField === "phone"}
              value={user.phone || "-"}
              field="phone"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Telefone Celular">
            <UpdateableItem
              loading={loadingField === "cellphone"}
              value={user.cellphone || "-"}
              field="cellphone"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="CPF">
            <UpdateableItem
              loading={loadingField === "cpf"}
              value={user.cpf || "-"}
              field="cpf"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Sobre">
            <UpdateableItem
              value={user.aboutMe || "-"}
              field="aboutMe"
              onChange={updateUserData}
            />
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Dados Empresariais
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Nome da Empresa">
            <UpdateableItem
              value={user.business || "-"}
              field="business"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="CNPJ">
            <UpdateableItem
              value={user.cnpj || "-"}
              field="cnpj"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Qtd. Funcionários">
            <UpdateableItem
              value={user.employees || "-"}
              field="employees"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Média de Faturamento">
            <UpdateableItem
              value={user.billing_average || "-"}
              field="billing_average"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Pontos fontes como empresário">
            <UpdateableItem
              value={user.strongPoints || "-"}
              field="strongPoints"
              onChange={updateUserData}
            />
          </Descriptions.Item>
          <Descriptions.Item label="Dúvidas como empreendedor">
            <UpdateableItem
              extraClass="full-width"
              value={user.entrepreneurDoubts || "-"}
              field="entrepreneurDoubts"
              onChange={updateUserData}
            />
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};
