import React, { useEffect, useState } from "react";
import { Col, Icon, Input, Popover, Row, Select, Table, Tag } from "antd";

import { createUsersListenerService } from "../../services/user";
import { notifyError } from "../../helpers/notifications";
import rolesEnum from "../../models/rolesEnum";
import Can from "../../rbac/Can";
import StateFilter from "../forms/StateFilter";
import UserOptions from "./UserOptions";
import UserDetails from "./UserDetails";
import UserDetailsModal from "./UserDetailsModal";

export default () => {
  const [users, setUsers] = useState(null);
  const [searchName, setSearchName] = useState(null);
  const [cpf, setCpf] = useState("");
  const [cnpj, setCnpj] = useState("");
  const [activeState, setActiveState] = useState(null);
  const [role, setRole] = useState(null);
  const [isSuspended, setIsSuspended] = useState(null);

  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getUsers();
  }, [activeState, role, cpf, cnpj, isSuspended]);

  const getUsers = async () => {
    try {
      isLoading(true);
      createUsersListenerService(
        { filters: { stateId: activeState, role, cpf, cnpj, isSuspended } },
        (users) => {
          setUsers(users);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
      width: "30%",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "20%",
      render: (email) => {
        if (email && email.includes(".appleid.com"))
          return (
            <Tag color="orange">Cliente não permite visualizar seu e-mail</Tag>
          );
        return <p>{email}</p>;
      },
    },
    {
      title: "Telefone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Estado",
      dataIndex: "stateUf",
      key: "state",
    },
    {
      title: "Status",
      key: "status",
      render: ({ isSuspended }) => (
        <Popover content={isSuspended ? "Perfil Desativado" : "Perfil Ativado"}>
          <Tag color={isSuspended ? "orange" : "green"}>
            <Icon type={isSuspended ? "login" : "logout"} />
          </Tag>
        </Popover>
      ),
    },
    {
      title: "",
      key: "options",
      width: "10%",
      render: (record) => <UserOptions user={record} />,
    },
  ];

  const usersFilter = () => {
    if (searchName && searchName !== "") {
      return users.filter((user) =>
        user.name.toLowerCase().includes(searchName.toLowerCase())
      );
    }
    return users;
  };

  return (
    <div>
      <UserDetailsModal />
      <Row gutter={24}>
        <Col span={4}>
          <Input.Search
            maxLength={15}
            onPressEnter={(v) => setSearchName(v.target?.value)}
            onSearch={(v) => setSearchName(v.target?.value)}
            allowClear
            placeholder="Nome"
          />
        </Col>
        <Can
          perform="users:filter:state"
          yes={() => (
            <Col span={4}>
              <StateFilter onSelect={(stateId) => setActiveState(stateId)} />
            </Col>
          )}
        />

        <Can
          perform="users:filter:role"
          yes={() => (
            <Col span={4}>
              <Select
                value={role}
                placeholder="Filtrar por nível de acesso"
                style={{ width: 220 }}
                onChange={(role) => setRole(role)}
                showSearch
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                }
              >
                <Select.Option value={null}>
                  Selecionar Nível de Acesso
                </Select.Option>
                <Select.Option value={rolesEnum.SUPER_ADMIN}>
                  Super Admin
                </Select.Option>
                <Select.Option value={rolesEnum.ADMIN}>Admin</Select.Option>
                <Select.Option value={rolesEnum.USER}>Usuários</Select.Option>
              </Select>
            </Col>
          )}
        />
        <Col span={4}>
          <Select
            value={isSuspended}
            placeholder="Filtrar por status"
            style={{ width: 220 }}
            onChange={(status) => setIsSuspended(status)}
            showSearch
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) >= 0
            }
          >
            <Select.Option value={null}>Selecionar Status</Select.Option>
            <Select.Option value={false}>Ativado</Select.Option>
            <Select.Option value={true}>Desativado</Select.Option>
          </Select>
        </Col>
        <Col span={4}>
          <Input.Search
            maxLength={15}
            onPressEnter={(v) => setCpf(v.target?.value)}
            onSearch={(v) => setCpf(v.target?.value)}
            allowClear
            placeholder="CPF"
          />
        </Col>
        <Col span={4}>
          <Input.Search
            maxLength={30}
            onPressEnter={(v) => setCnpj(v.target?.value)}
            onSearch={(v) => setCnpj(v.target?.value)}
            allowClear
            placeholder="CNPJ"
          />
        </Col>
      </Row>
      <Row style={{ marginTop: 24 }} gutter={24}>
        <Col span={24}>
          <Table
            columns={columns}
            dataSource={usersFilter()}
            loading={loading}
            rowExpandable
            expandedRowRender={(user) => <UserDetails user={user} />}
          />
        </Col>
      </Row>
    </div>
  );
};
