import React, { useState } from "react";
import { Modal } from "antd";
import DropdownMenu from "../../components/DropdownMenu";
import Spin from "../../components/Spin";
import {
  deleteUserService,
  sendEmailOnUserApprovalService,
  updateUserService,
} from "../../services/user";
import { notifySuccess } from "../../helpers/notifications";
import { userCan } from "../../rbac/Can";

const { confirm } = Modal;

export default ({ user }) => {
  const [loading, isLoading] = useState(false);

  const suspendUser = async () => {
    try {
      isLoading(true);
      const isSuspended = !user.isSuspended;
      await updateUserService(user.id, { ...user, isSuspended });

      if (!isSuspended) {
        await sendEmailOnUserApprovalService(user.id);
      }

      const status = isSuspended ? "desativado" : "ativado";

      notifySuccess({ title: `Usuário ${status}` });
      isLoading(false);
    } catch (e) {
      isLoading(false);
    }
  };

  const promptToDeleteUser = async () => {
    confirm({
      okText: "Quero excluir",
      title: "Você tem certeza de que deseja excluir este usuário?",
      content:
        'Clicando em "Quero excluir" TODAS AS NOTÍCIAS, EVENTOS E POSTS NO FÓRUM que ela criou SERÃO EXCLUÍDAS. Tem certeza de que quer fazer isso?',
      onOk() {
        deleteUser();
      },
      onCancel() {
        isLoading(false);
      },
    });
  };

  const deleteUser = async () => {
    try {
      isLoading(true);
      await deleteUserService(user.id);
      notifySuccess({
        title:
          "Usuário deletado. Em breve as informações desta pessoa serão excluídas",
      });
      isLoading(false);
    } catch (e) {
      isLoading(false);
    }
  };

  if (loading) return <Spin />;

  const options = [
    {
      label: user.isSuspended ? "Ativar Usuário" : "Desativar Usuário",
      onSelect: suspendUser,
    },
  ];

  if (userCan({ perform: "users:delete_user" }))
    options.push({
      label: "Deletar Usuário",
      onSelect: promptToDeleteUser,
    });

  return <DropdownMenu label="Opções" options={options} />;
};
