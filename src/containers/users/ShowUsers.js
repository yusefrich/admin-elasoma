import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Card,
  Select,
  Tag,
  Upload,
} from "antd";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import Editor from "../../components/Editor";
import htmlToDraft from "html-to-draftjs";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import { firestoreTimeToMoment, formattedDate } from "../../helpers/common";
import styled from "styled-components";

const { Option } = Select;

const DiscountContainer = styled.div`
display: flex;
justify-content: between;
margin-bottom: 15px;
margin-left: 15px;
margin-right: 15px;
cursor: pointer;
`;
const DiscountTitleContainer = styled.div`
margin-left: 20px
`;
const DiscountTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
color: #9096B4;
`;
const DiscountSubTitle = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 16px;
line-height: 175.9%;
color: #9096B4;
`;


const DrawerContentDetail = styled.div`
text-align: center;
margin: 15px;
`;
const DetailTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: bold;
font-size: 22px;
line-height: 120%;
margin-bottom: 15px;
color: #9096B4;
`;
const DetailParagraph = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 14px;
line-height: 120%;
margin-bottom: 56px;
color: #9096B4;

`;
const DetailShoTitle = styled.h4`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 18px;
line-height: 240%;
margin-bottom: 16px;
color: #9096B4;

`;
const DetailShoLocal = styled.p`
font-family: Poppins;
font-style: normal;
font-weight: 300;
font-size: 14px;
line-height: 120%;
color: #9096B4;

`;
export default ({
  affiliated,
}) => {
  const [drawerOpen, isDrawerOpen] = useState(false);
  const [areas, setAreas] = useState([]);
  const [loading, isLoading] = useState(false);
  const [buttonDisabled, isButtonDisabled] = useState(false);
  const [uploadingImage, isUploadingImage] = useState(false);
  const [windowWidth, setWindowWidth] = useState("50%");
  const [imageUrl, setImageUrl] = useState(affiliated.image);
  const { states } = useSelector((state) => state.states);


  const IncreaseWindowSizeButton = () => {
    if (windowWidth === 520)
      return (
        <Icon
          type="fullscreen"
          onClick={() => setWindowWidth("50%")}
          style={{ marginRight: 20 }}
        />
      );
    else
      return (
        <Icon
          type="fullscreen-exit"
          onClick={() => setWindowWidth(520)}
          style={{ marginRight: 20 }}
        />
      );
  };

  const onCancelForm = async () => {
    isDrawerOpen(false);
    setImageUrl("");
  };


  return (
    <>
      <DiscountContainer onClick={() => isDrawerOpen(!drawerOpen)} >
        <img style={{ borderRadius: 90, marginTop: 20 }} src={affiliated.photoURL} width="60px" height="60px" alt="" />
        <DiscountTitleContainer>
          <DiscountTitle>{affiliated.name}</DiscountTitle>
          <DiscountSubTitle>{affiliated.business}</DiscountSubTitle>
        </DiscountTitleContainer>
      </DiscountContainer>
      <Divider style={{ background: "#090C1D" }} />

      <Drawer
        title={[<IncreaseWindowSizeButton />]}
        width={windowWidth}
        onClose={onCancelForm}
        visible={drawerOpen}
        drawerStyle={{ background: "#151A2E"}}
        bodyStyle={{ paddingBottom: 80, background: "#151A2E" }}
        headerStyle={{ background: "#9096B4", borderBottom: "1px solid #090C1D", color: "#9096B4" }}
      >
        <DrawerContentDetail>

          <img style={{ borderRadius: 300, marginTop: 20, marginBottom: 30 }} src={affiliated.photoURL} width="260px" height="260px" alt="" />

          <DetailTitle> {affiliated.name}</DetailTitle>
          <DetailParagraph> {affiliated.business} </DetailParagraph>
          <DetailShoTitle> {affiliated.cityName} / {affiliated.stateUf} </DetailShoTitle>
          <DetailShoLocal> {affiliated.aboutMe} </DetailShoLocal>

        </DrawerContentDetail>
      </Drawer>
    </>
  );
};