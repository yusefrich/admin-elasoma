import React, { useEffect, useState } from "react";
import { Avatar, Col, Descriptions, Divider, Row, Table } from "antd";
import { notifyError } from "../../helpers/notifications";
import { rolesDisplay } from "../../models/rolesEnum";
import { userCan } from "../../rbac/Can";
import { getUserFromStore } from "../../helpers/store";
import StateFilter from "../forms/StateFilter";
import { createNewsletterSubscribersListener } from "../../services/notifications";

export default () => {
  const [subscribers, setSubscribers] = useState(null);
  const [activeState, setActiveState] = useState(null);

  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getSubscribers();
  }, [activeState]);

  const getSubscribers = async () => {
    try {
      isLoading(true);
      createNewsletterSubscribersListener(
        { filters: { stateId: activeState } },
        (subscribers) => {
          setSubscribers(subscribers);
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
      width: "30%",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "20%",
    },
    {
      title: "Telefone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Estado",
      dataIndex: "stateUf",
      key: "state",
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col span={4}>
          <StateFilter onSelect={(stateId) => setActiveState(stateId)} />
        </Col>
      </Row>
      <Divider />
      <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={subscribers}
            loading={loading}
            rowExpandable
            expandedRowRender={(user) => <UserDetails user={user} />}
          />
        </Col>
      </Row>
    </div>
  );
};

const UserDetails = ({ user }) => {
  const loggedUser = getUserFromStore();

  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          <Avatar
            size="large"
            src={user.photoURL}
            style={{ marginRight: "16px" }}
          />
          {user.name}
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          {userCan({ role: loggedUser.role, perform: "user:show_role" }) && (
            <Descriptions.Item label="Nível">
              {rolesDisplay[user.role]}
            </Descriptions.Item>
          )}
          {user.email && (
            <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
          )}
          {user.stateUf && (
            <Descriptions.Item label="UF">{user.stateUf}</Descriptions.Item>
          )}
          {user.cityName && (
            <Descriptions.Item label="Cidade">
              {user.cityName}
            </Descriptions.Item>
          )}

          {user.phone && (
            <Descriptions.Item label="Telefone">{user.phone}</Descriptions.Item>
          )}
          {user.cellphone && (
            <Descriptions.Item label="Telefone Celular">
              {user.cellphone}
            </Descriptions.Item>
          )}
          {user.cpf && (
            <Descriptions.Item label="CPF">{user.cpf}</Descriptions.Item>
          )}
          {user.aboutMe && (
            <Descriptions.Item label="Sobre">{user.aboutMe}</Descriptions.Item>
          )}
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Dados Empresariais
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          {user.business && (
            <Descriptions.Item label="Nome da Empresa">
              {user.business}
            </Descriptions.Item>
          )}
          {user.cnpj && (
            <Descriptions.Item label="CNPJ">{user.cnpj}</Descriptions.Item>
          )}
          {user.employees && (
            <Descriptions.Item label="Qtd. Funcionários">
              {user.employees}
            </Descriptions.Item>
          )}
          {user.billing_average && (
            <Descriptions.Item label="Média de Faturamento">
              {user.billing_average}
            </Descriptions.Item>
          )}
          {user.strongPoints && (
            <Descriptions.Item label="Pontos fontes como empresário">
              {user.strongPoints}
            </Descriptions.Item>
          )}
          {user.entrepreneurDoubts && (
            <Descriptions.Item label="Dúvidas como empreendedor">
              {user.entrepreneurDoubts}
            </Descriptions.Item>
          )}
        </Descriptions>
      </Col>
    </Row>
  );
};
