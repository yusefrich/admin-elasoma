import React, { useEffect, useState } from "react";
import { Col, Icon, Popconfirm, Row, Table, Tag } from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import UpdateableItem from "../../components/UpdateableItem";
import {
  createForumCategoriesListener,
  deleteForumCategoryService,
  updateForumCategoryCategoryService,
} from "../../services/forum/forumCategories";

export default () => {
  const [categories, setCategories] = useState("");
  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getCategories();
  }, []);

  const getCategories = async () => {
    try {
      isLoading(true);
      createForumCategoriesListener((categories) => {
        setCategories(categories);
        isLoading(false);
      });
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Categoria",
      dataIndex: "name",
      key: "name",
      width: "50%",
    },
    {
      title: "",
      dataIndex: "",
      key: "",
      render: (category) => (
        <DeleteAreaButton onDelete={getCategories} category={category} />
      ),
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col>
          <Table
            columns={columns}
            dataSource={categories}
            loading={loading}
            rowExpandable
            expandedRowRender={(category) => (
              <CategoryDetails onUpdate={getCategories} category={category} />
            )}
          />
        </Col>
      </Row>
    </div>
  );
};

const CategoryDetails = ({ category, onUpdate }) => {
  const [loadingName, setLoadingName] = useState(false);

  const updateArea = async (value) => {
    try {
      setLoadingName(true);
      await updateForumCategoryCategoryService(category, value);
      onUpdate();
      notifySuccess({ title: "Tudo certo", message: "Categoria Atualizada" });
      setLoadingName(false);
    } catch (e) {
      notifyError(e);
      setLoadingName(false);
    }
  };

  return (
    <>
      <Row gutter={16}>
        <Col span={24}>
          <h4>Detalhes da Categoria</h4>
        </Col>
        <Col span={6}>
          <span>Nome: </span>
          <UpdateableItem
            type="input"
            field="name"
            loading={loadingName}
            onChange={updateArea}
            value={category.name}
          />
          <br />
          <small>Clique na categoria para alterar </small>
        </Col>
      </Row>
    </>
  );
};

const DeleteAreaButton = ({ category, onDelete }) => {
  const [loading, setLoading] = useState(false);

  const deleteCategory = async () => {
    try {
      setLoading(true);
      await deleteForumCategoryService(category);
      onDelete();
      notifySuccess({ title: "Tudo certo", message: "Categoria removida" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Todas as postagens já enviadas
          para esta categoria
          <br /> também serão excluídas
        </p>
      }
      onConfirm={deleteCategory}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red"> {loading && <Icon type="loading" />} excluir</Tag>
    </Popconfirm>
  );
};
