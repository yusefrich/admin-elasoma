import React, { useEffect, useState } from "react";
import {
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Row,
  Select,
  Spin,
  Tag,
} from "antd";
import { notifyError, notifySuccess } from "../../../helpers/notifications";
import {
  createPostsListener,
  deleteForumPostService,
  getActiveForumPostsByCategoryService,
} from "../../../services/forum/forumPosts";
import ForumCategoriesSelect from "../ForumCategoriesSelect";
import EditForumPostForm from "../EditForumPostForm";
import ShowForumPosts from "../ShowForumPosts";
import UserAddForumPostForm from "../UserAddForumPostForm";
import StateFilter from "../../forms/StateFilter";
import { getForumCategoriesService } from "../../../services/forum/forumCategories";

const { Option } = Select;

export default () => {
  const [posts, setPosts] = useState([]);
  const [loading, isLoading] = useState(false);
  const [isActive, setActiveFilter] = useState("all");
  const [activeState, setActiveState] = useState(null);
  const [category, setCategory] = useState("all");
  const [postCategories, setCategories] = useState([]);

  useEffect(() => {
    /* getPosts(); */
    getPostsByCategory();
    getForumCategories();
  }, [isActive, category, activeState]);

  const getPosts = async () => {
    try {
      isLoading(true);
      createPostsListener(
        { filters: { isActive, categoryId: category, stateId: activeState } },
        (categories) => {
          setPosts(categories);
          console.log("those are the posts");
          console.log(categories)
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };
  const getPosts2 = async () => {
    try {
      isLoading(true);
      createPostsListener(
        { filters: { isActive, categoryId: category, stateId: activeState } },
        (categories) => {
          setPosts(categories);
          console.log("those are the posts");
          console.log(categories)
          isLoading(false);
        }
      );
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };
  const getPostsByCategory = async () => {
    try {
      isLoading(true);
      const posts = await getActiveForumPostsByCategoryService(
        category,
        activeState,
      );
      setPosts(posts);
      console.log("those are the posts");
      console.log(posts)

      isLoading(false);
    } catch (e) {
      isLoading(false);
    }
  };
  const getForumCategories = async () => {
    try {
      const postCats = await getForumCategoriesService();
      console.log("postCats")
      console.log(postCats)
      setCategories(postCats);
    } catch (e) { }
  };


  const columns = [
    {
      title: "Título",
      dataIndex: "title",
      key: "title",
      width: "30%",
    },
    {
      title: "Texto",
      dataIndex: "text",
      key: "text",
      width: "50%",
    },
    {
      title: "Categoria",
      dataIndex: "category.name",
      key: "category.id",
      width: "10%",
    },
    {
      title: "Ativo",
      key: "id",
      width: "5%",
      render: (post) => {
        const iconProperties = post.isActive
          ? { type: "check-circle", style: { color: "green" }, theme: "filled" }
          : {
            type: "close-circle",
            style: { color: "red" },
          };
        return <Icon {...iconProperties} />;
      },
    },
    {
      title: "",
      dataIndex: "",
      key: "",
      render: (post) => (
        <>
          <EditForumPostForm post={post} />
          <DeletePostButton post={post} />
        </>
      ),
    },
  ];
  const ForumList = () => (
    <>
      {posts.map((row) => {
        if (row.isActive) {
          return (<ShowForumPosts key={"forumList"+row.id} post={row} />)
        } else {
          return null
        }
      }
      )}
    </>
  );

  function CheckIfPopulated(value){
    var return_value = false;
    posts.forEach(element => {
      if(element.category.id === value.id){
        return_value = true;
      }
    });
    return return_value;
  }
   
  return (
    <div>
      <Row gutter={24}>
        {/* <Col span={4}>
          <StateFilter onSelect={(stateId) => setActiveState(stateId)} />
        </Col> */}
        <Col style={{display: 'flex'}} span={24}>{/* postCategories */}
          {postCategories &&
            <div style={{marginRight: 20}} className="user_select">

              <Select defaultValue={"all"} onChange={(value) => setCategory(value)}>
                <Option value="all">Todas as categorias</Option>
                {postCategories.map((category) => (
                  <Option hidden={!CheckIfPopulated(category)} value={category.id}>{category.name}</Option>
                ))}
              </Select>
            </div>}

          <UserAddForumPostForm />
        </Col>
      </Row>
      <Row gutter={24}>
        <Divider style={{ background: "#090C1D" }} />
        <Col>
        <div hidden={!loading} style={{textAlign: "center"}}>
        <Spin className="soma-loading" tip="Carregando...">{/* hidden={!loading} */}
        </Spin>
      </div>

          {posts && <ForumList />}

          
        </Col>
      </Row>
    </div>
  );
};

const PostDetails = ({ post }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Fórum Post
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Título">{post.title}</Descriptions.Item>
        </Descriptions>
        {post.text && (
          <Descriptions layout="vertical" bordered size="small">
            <Descriptions.Item label="Texto">{post.text}</Descriptions.Item>
          </Descriptions>
        )}
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Categoria">
            <Tag color="blue">{post.category?.name}</Tag>
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Estado">
            {post.stateDetails.name}
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Autoria
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Autor">
            {post.author.name}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Empresa">
            {post.author.business}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};

const DeletePostButton = ({ post }) => {
  const [loading, setLoading] = useState(false);

  const deleteForumPost = async () => {
    try {
      setLoading(true);
      await deleteForumPostService(post.id);
      notifySuccess({ title: "Tudo certo", message: "Post removido" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Todas as respostas já enviadas
          para este post
          <br /> também serão excluídas
        </p>
      }
      onConfirm={deleteForumPost}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red"> {loading && <Icon type="loading" />} excluir</Tag>
    </Popconfirm>
  );
};
