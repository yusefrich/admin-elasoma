import React from "react";
import { useSelector } from "react-redux";
import { Select } from "antd";
const { Option } = Select;

export default ({ onChange, value }) => {
  const { categories } = useSelector((store) => store.forum);

  return (
    <Select defaultValue={value} onChange={onChange}>
      <Option value="all">Todas as categorias</Option>
      {categories.map((category) => (
        <Option value={category.id}>{category.name}</Option>
      ))}
    </Select>
  );
};
