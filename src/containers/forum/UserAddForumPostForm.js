import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Switch,
} from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { createForumPostService } from "../../services/forum/forumPosts";
import { getForumCategoriesService } from "../../services/forum/forumCategories";
import UserStateFilter from "../forms/UserStateFilter";
import { getUserFromStore } from "../../helpers/store";
import rolesEnum from "../../models/rolesEnum";
import styled from "styled-components";

const AddButton = styled.button`
font-family: Poppins;
font-style: normal;
font-weight: 500;
font-size: 14px;
line-height: 220%;
color: #9096B4;
padding-right: 40px;
background: transparent;
border: 0px;
border-radius: 0;
box-shadow: none;
border-bottom: 1px solid #9096B4;
cursor: pointer;
`;

export default Form.create({ name: "add_forum_post" })(
  ({
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      resetFields,
      setFieldsValue,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [categories, setCategories] = useState([]);
    const [titleLimite, setTitleLimit] = useState([]);
    const [contentLimite, setContentLimit] = useState([]);
    const user = getUserFromStore();

    useEffect(() => {
      getForumCategories();
      setTitleLimit(0);
      setContentLimit(0);
      
    }, []);

    const getForumCategories = async () => {
      try {
        const categories = await getForumCategoriesService();
        setCategories(categories);

      } catch (e) {}
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            await createForumPostService(values);
            notifySuccess({
              title: "Ok!",
              message:
                "Uma nova postagem para o forum foi criada e já está ativa",
            });
            resetFields();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };
    function maxLengthCheck(object){
      console.log("working")
      console.log(object);
      if (object.currentTarget.value.length > object.currentTarget.maxLength) {
        object.currentTarget.value = object.currentTarget.value.slice(0, object.currentTarget.maxLength)
      }

      setTitleLimit(object.currentTarget.value.length)

    }
    function maxLengthCheckContent(object){
      console.log("working")
      console.log(object);
      if (object.currentTarget.value.length > object.currentTarget.maxLength) {
        object.currentTarget.value = object.currentTarget.value.slice(0, object.currentTarget.maxLength)
      }

      setContentLimit(object.currentTarget.value.length)

    }
  

    return (
      <>
        <AddButton onClick={() => isDrawerOpen(!drawerOpen)}>
          Enviar uma ideia
        </AddButton>
        <Drawer
          title="Enviar uma nova ideia"
          onClose={() => isDrawerOpen(false)}
          width={520}
          visible={drawerOpen}
          drawerStyle={{ background: "#090C1D"}}
          bodyStyle={{ paddingBottom: 80, background: "#090C1D" }}/* 151A2E */
          headerStyle={{ background: "#9096B4", borderBottom: "1px solid #090C1D", color: "#9096B4" }}
        >
          <div className="user_post_form">

          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Título">
                  {getFieldDecorator("title", {
                    rules: [
                      {
                        required: true,
                        message: "Insira um títullo",
                      },
                    ],
                  })(<Input max={100} maxLength = "100"  onInput={maxLengthCheck} />)}
                </Form.Item>
                <p  style={{position: "relative", top: -16, color: "#9096B4"}} >{titleLimite}/100</p>
                <Form.Item label="Texto (opcional)">
                  {getFieldDecorator("text", { initialValue: "" })(
                    <Input maxLength={250} onInput={maxLengthCheckContent} />
                  )}
                </Form.Item>
                <p  style={{position: "relative", top: -16, color: "#9096B4"}} >{contentLimite}/250</p>
                <Divider style={{background: '#151A2E'}} />
                <Form.Item hidden label="Estado">
                  {getFieldDecorator("stateId", {
                    rules: [{ required: true, message: "Selecione um estado" }],
                  })(
                      <UserStateFilter className="user_select"
                        disabled={user._role !== rolesEnum.SUPER_ADMIN}
                        initialValue={user._state._id}
                        onSelect={(stateId) => setFieldsValue({ stateId })}
                        />
                  )}
                </Form.Item>
                <Form.Item label="Categoria">
                  {getFieldDecorator("category", {
                    rules: [
                      { required: true, message: "Selecione uma categoria" },
                    ],
                  })(
                    <div className="user_select">

                    <Select>
                      {categories.map((category) => (
                        <Select.Option key={category.id} value={category.id}>
                          {category.name}
                        </Select.Option>
                      ))}
                    </Select>
                    </div>

                  )}
                </Form.Item>
                <Form.Item label="Enviar notificações de resposta da postagem">
                  {getFieldDecorator("receiveNotification", {
                    initialValue: false,
                  })(<Switch />)}
                </Form.Item>
              </Col>
            </Row>
            <Button loading={loading} htmlType="submit" type="primary">
              Salvar
            </Button>
          </Form>
          </div>

        </Drawer>
      </>
    );
  }
);
