import React, { useState } from "react";
import { Button, Col, Drawer, Form, Icon, Input, Row } from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { createNewForumCategory } from "../../services/forum/forumCategories";

export default Form.create({ name: "add_forum_category" })(
  ({ form: { getFieldDecorator, validateFieldsAndScroll, resetFields } }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);

            await createNewForumCategory(values);
            notifySuccess({
              title: "Ok!",
              message: "Uma nova categoria para o forum foi criada ",
            });
            resetFields();
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    return (
      <>
        <Button type="primary" onClick={() => isDrawerOpen(!drawerOpen)}>
          <Icon type="plus" /> Nova Categoria
        </Button>
        <Drawer
          title="Criar Nova Categoria"
          onClose={() => isDrawerOpen(false)}
          width={520}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Categoria">
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Insira um nome para a categoria",
                      },
                    ],
                  })(<Input />)}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button
                onClick={() => isDrawerOpen(false)}
                style={{ marginRight: 8 }}
              >
                Cancelar
              </Button>
              <Button loading={loading} htmlType="submit" type="primary">
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
