import React, { useEffect, useState } from "react";
import {
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Row,
  Alert,
  Select,
  Table,
  Card,
  Tag,
  Spin,
  Typography,
} from "antd";
import { notifyError, notifySuccess } from "../../../helpers/notifications";
import Can from "../../../rbac/Can";
import { getUserFromStore } from "../../../helpers/store";
import {
  deleteNewsService,
  getNewsByStateService,
  getNewsService,
} from "../../../services/news";
import { useSelector } from "react-redux";
import ShowNews from "../ShowNews";
import { firestoreTimeToMoment, formattedDate } from "../../../helpers/common";
import styled from "styled-components";

const { Option } = Select;
const { Paragraph } = Typography;

const CardTitle = styled.h2`
font-family: Poppins;
font-style: normal;
font-weight: 600;
font-size: 22px;
line-height: 110%;
color: #9096B4;
`;

export default () => {
  const [news, setNews] = useState(null);
  const [slicedNews, setSlicedNews] = useState(null);
  const [loading, isLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(5);
  const user = getUserFromStore();
  const { states } = useSelector((store) => store.states);

  useEffect(() => {
    getNews();
  }, []);

  const getNews = async (stateId) => {
    try {
      const getAllNews = async () => {
        isLoading(true);
        const news = stateId
          ? await getNewsByStateService(stateId)
          : await getNewsService({
            filters: { stateId: null, highlight: null },
          });

        var filteredNews = [];

        news.forEach(n => {
          n.category.forEach(nc => {
            if (user._interests) {
              user._interests.forEach(i => {
                if (nc === i.id) { filteredNews.push(n) }
              });
            }
          });
        });
        setNews(filteredNews);
        slicePosts(news, 1);
        isLoading(false);
      };
      /* const getNewsByState = async () => {
        isLoading(true);
        const news = await getNewsByStateService(user._state._id);
        setNews(news);
        isLoading(false);
      }; */

      getAllNews();
      // getNewsByState();

    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };
  const slicePosts = (allNews, currentPage) => {
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = allNews.slice(indexOfFirstPost, indexOfLastPost);

    setSlicedNews(currentPosts);
  }

  const toPage = async (number) => {
    setCurrentPage(number);
    slicePosts(news, number);
  }
  const getIsSelected = (value) => {
    if (value === currentPage) {

      return ({ textDecoration: "underline", textUnderlinePosition: "under" })
    }
  }


  const NewsCards = () => (
    <>
      {slicedNews.map((row) => (
        <ShowNews key={row.id} news={row} />

      ))}
    </>
  );

  const Pagination = () => {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(news.length / postsPerPage); i++) {
      pageNumbers.push(i);
    }
    return (
      <div className="pagination  justify-content-center pagination-margin">
        <ul className="pagination">
          <li key={"prev"} className="page-item">
            <button onClick={() => { toPage(currentPage - 1) }} className="page-link">anterior</button>
          </li>

          {pageNumbers.map((row) => (
            <li key={row} className="page-item">
              <button onClick={() => { toPage(row) }} style={getIsSelected(row)} className="page-link">
                {row}
              </button>
            </li>
          ))}

          <li key={"next"} className="page-item">
            <button onClick={() => { toPage(currentPage + 1) }} className="page-link">próxima</button>
          </li>
        </ul>
      </div>
    )
  };


  return (
    <div>
      <Row gutter={24}>
        <Col>
          <div hidden={!loading} style={{ textAlign: "center" }}>
            <Spin className="soma-loading" tip="Carregando...">{/* hidden={!loading} */}
            </Spin>
          </div>

          {slicedNews && <NewsCards />}
        </Col>
      </Row>
      {slicedNews && <Pagination />}
    </div>
  );
};


