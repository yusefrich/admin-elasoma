import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Divider,
  Drawer,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Switch,
  Tag,
  Upload,
} from "antd";
import { getAreasOfInterestsService } from "../../services/interests";
import { uploadFileService } from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { updateNewsService } from "../../services/news";
import { useSelector } from "react-redux";
import { userCan } from "../../rbac/Can";
import { beforeUpload } from "../../helpers/forms";
import Editor from "../../components/Editor";
import htmlToDraft from "html-to-draftjs";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import draftToHtml from "draftjs-to-html";

const { Option } = Select;

export default Form.create({ name: "add_news_form" })(
  ({
    news,
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      validateFields,
      getFieldValue,
      resetFields,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [areas, setAreas] = useState([]);
    const [loading, isLoading] = useState(false);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [windowWidth, setWindowWidth] = useState("90%");
    const [imageUrl, setImageUrl] = useState(news.image);
    const { states } = useSelector((state) => state.states);

    const user = getUserFromStore();

    useEffect(() => {
      getAreas();
    }, []);

    const getAreas = async () => {
      const areas = await getAreasOfInterestsService();
      setAreas(areas);
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            if (imageUrl) values.image = imageUrl;

            values.author = user.toJson();
            values.authorUid = user.id;
            values.updatedAt = new Date();
            values.text = draftToHtml(
              convertToRaw(values.text.getCurrentContent())
            );

            await updateNewsService(news.id, values);
            notifySuccess({
              title: "Ok!",
              message: "A notícia foi atualizada",
            });
            window.location.reload();
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: `news/${user.id}`,
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const IncreaseWindowSizeButton = () => {
      if (windowWidth === 520)
        return (
          <Icon
            type="fullscreen"
            onClick={() => setWindowWidth("90%")}
            style={{ marginRight: 20 }}
          />
        );
      else
        return (
          <Icon
            type="fullscreen-exit"
            onClick={() => setWindowWidth(520)}
            style={{ marginRight: 20 }}
          />
        );
    };

    const onCancelForm = async () => {
      isDrawerOpen(false);
      setImageUrl("");
    };

    const contentBlock = htmlToDraft(news.text);
    const contentState = ContentState.createFromBlockArray(
      contentBlock.contentBlocks
    );
    const newsText = EditorState.createWithContent(contentState);

    return (
      <>
        <Tag
          color="blue"
          type="primary"
          onClick={() => isDrawerOpen(!drawerOpen)}
        >
          <Icon type="edit" /> Editar
        </Tag>
        <Drawer
          title={[<IncreaseWindowSizeButton />, "Criar notícia"]}
          width={windowWidth}
          onClose={onCancelForm}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Titulo">
                  {getFieldDecorator("title", {
                    initialValue: news.title,
                    rules: [{ required: true, message: "Insira um título" }],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Texto de Destaque">
                  {getFieldDecorator("spotlight", {
                    initialValue: news.spotlight,
                  })(<Input style={{ width: "100%" }} />)}
                </Form.Item>
                <Form.Item label="Texto">
                  {getFieldDecorator("text", {
                    initialValue: newsText,
                    rules: [{ required: true, message: "Insira um texto" }],
                  })(<Editor />)}
                </Form.Item>
                <Form.Item label="Imagem de Destaque">
                  {getFieldDecorator("image", { initialValue: news.image })(
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                      customRequest={onUpload}
                      onChange={handleChangeImage}
                    >
                      {imageUrl ? (
                        <img
                          src={imageUrl}
                          alt="avatar"
                          style={{ width: "100%" }}
                        />
                      ) : (
                        <div>
                          <Icon type={uploadingImage ? "loading" : "plus"} />
                          <div className="ant-upload-text">Upload</div>
                        </div>
                      )}
                    </Upload>
                  )}
                </Form.Item>
                <Divider />
                <Form.Item label="Destaque">
                  {getFieldDecorator("highlight", {
                    initialValue: news.highlight || false,
                    valuePropName: "checked",
                  })(<Switch />)}
                </Form.Item>
                <Form.Item label="Categoria">
                  {getFieldDecorator("category", {
                    initialValue: news.category[0],
                    rules: [
                      { required: true, message: "Selecione uma categoria" },
                    ],
                  })(
                    <Select>
                      {areas.map((area) => (
                        <Option key={area.id} value={area.id}>
                          {area.name.toUpperCase()}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                <Form.Item label="Estado">
                  {getFieldDecorator("state", {
                    initialValue: news.state,
                    rules: [
                      { required: true, message: "Selecione uma estado" },
                    ],
                  })(
                    <Select disabled={!userCan({ perform: "news:edit_state" })}>
                      {states.map((state) => (
                        <Option key={state.id} value={state.id}>
                          {state.name}
                        </Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button onClick={onCancelForm} style={{ marginRight: 8 }}>
                Cancelar
              </Button>
              <Button
                disabled={buttonDisabled}
                loading={loading}
                htmlType="submit"
                type="primary"
              >
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
