import React, { useEffect, useState } from "react";
import { Col, Icon, Row, Table, Tag, Upload, Popconfirm } from "antd";
import { SliderPicker } from "react-color";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import {
  deleteAreaOfInterestService,
  getAreasOfInterestsService,
  updateAreaOfInterestService,
} from "../../services/interests";
import UpdateableItem from "../../components/UpdateableItem";
import { beforeUpload } from "../../helpers/forms";
import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";

export default () => {
  const [areas, setAreas] = useState("");
  const [loading, isLoading] = useState(false);

  useEffect(() => {
    getAreas();
  }, []);

  const getAreas = async () => {
    try {
      isLoading(true);
      const areas = await getAreasOfInterestsService();
      setAreas(areas);
      isLoading(false);
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
      width: "50%",
    },
    {
      title: "Cor",
      key: "color",
      width: "20%",
      render: (area) => <Tag color={area.color}>{area.color}</Tag>,
    },
    {
      title: "Imagem de Destaque",
      dataIndex: "image",
      key: "image",
      render: (text) => <img src={text} width={100} />,
    },
    {
      title: "",
      dataIndex: "",
      key: "",
      render: (area) => <DeleteAreaButton onDelete={getAreas} area={area} />,
    },
  ];

  return (
    <div>
      <Row gutter={24}>
        <Col>
          <Table
            columns={columns}
            dataSource={areas}
            loading={loading}
            rowExpandable
            expandedRowRender={(area) => (
              <AreaDetails onUpdate={getAreas} area={area} />
            )}
          />
        </Col>
      </Row>
    </div>
  );
};

const AreaDetails = ({ area, onUpdate }) => {
  const [loadingName, setLoadingName] = useState(false);
  const [isPickerOpened, setPickerOpened] = useState(false);
  const [color, setColor] = useState(area.color);
  const [uploadingImage, isUploadingImage] = useState(false);

  const updateArea = async (value) => {
    try {
      setLoadingName(true);
      await updateAreaOfInterestService(area, value);
      onUpdate();
      notifySuccess({ title: "Tudo certo", message: "Área Atualizada" });
      setLoadingName(false);
    } catch (e) {
      notifyError(e);
      setLoadingName(false);
    }
  };

  const onUpload = async ({ onSuccess, onError, file, onProgress }) => {
    const onSuccessCallback = async (image) => {
      deleteImageFromUrlService(area.image);
      updateArea({ image });
      onSuccess("done");
    };
    uploadFileService({
      url: `areas_of_interests`,
      onSuccessCallback,
      onError,
      file,
      onProgress,
    });
  };

  const handleChangeImage = (info) => {
    if (info.file.status === "uploading") {
      isUploadingImage(true);
      return;
    }
    if (info.file.status === "done") {
      isUploadingImage(false);
    }
  };

  return (
    <>
      <Row gutter={16}>
        <Col span={24}>
          <h4>Detalhes da categoria</h4>
        </Col>
        <Col span={6}>
          <span>Nome: </span>
          <UpdateableItem
            type="input"
            field="name"
            loading={loadingName}
            onChange={updateArea}
            value={area.name}
          />
          <br />
          <small>Clique na categoria para alterar </small>
        </Col>
        <Col span={6}>
          <span>Cor: </span>
          <Tag
            color={area.color}
            onClick={() => setPickerOpened(!isPickerOpened)}
          >
            {area.color} - <small>Clique para alterar </small>
          </Tag>

          {isPickerOpened && (
            <div style={{ marginTop: 10 }}>
              <SliderPicker
                color={color}
                onChangeComplete={({ hex }) => setColor(hex)}
              />
              <Tag
                style={{ marginTop: 10 }}
                color="blue"
                onClick={() => updateArea({ color })}
              >
                Salvar
              </Tag>
            </div>
          )}
        </Col>
        <Col span={6}>
          <span>Imagem: </span>

          <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={beforeUpload}
            customRequest={onUpload}
            onChange={handleChangeImage}
          >
            {area.image ? (
              <img src={area.image} alt="avatar" style={{ width: "100%" }} />
            ) : (
              <div>
                <Icon type={uploadingImage ? "loading" : "plus"} />
                <div className="ant-upload-text">Upload</div>
              </div>
            )}
          </Upload>
        </Col>
      </Row>
    </>
  );
};

const DeleteAreaButton = ({ area, onDelete }) => {
  const [loading, setLoading] = useState(false);

  const deleteNews = async () => {
    try {
      setLoading(true);
      await deleteAreaOfInterestService(area);
      onDelete();
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Todas as notícias relacionadas a
          esta categoria
          <br /> também serão excluídas e não poderão ser encontradas
        </p>
      }
      onConfirm={deleteNews}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red"> {loading && <Icon type="loading" />} excluir</Tag>
    </Popconfirm>
  );
};
