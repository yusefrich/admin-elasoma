import React, { useEffect, useState } from "react";
import {
  Col,
  Descriptions,
  Divider,
  Icon,
  Popconfirm,
  Row,
  Select,
  Switch,
  Table,
  Tag,
  Typography,
} from "antd";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import Can from "../../rbac/Can";
import { deleteNewsService, getNewsService } from "../../services/news";
import { useSelector } from "react-redux";
import EditNewsForm from "./EditNewsForm";
import { firestoreTimeToMoment, formattedDate } from "../../helpers/common";

const { Option } = Select;
const { Paragraph } = Typography;

export default () => {
  const [news, setNews] = useState(null);
  const [highlightsFilter, setHighlightsFilter] = useState(false);
  const [stateFilter, setStateFilter] = useState(null);
  const [loading, isLoading] = useState(false);
  const { states } = useSelector((store) => store.states);

  useEffect(() => {
    getNews();
  }, [highlightsFilter, stateFilter]);

  const getNews = async () => {
    try {
      isLoading(true);
      const news = await getNewsService({
        filters: { stateId: stateFilter, highlight: highlightsFilter },
      });

      setNews(news);
      isLoading(false);
    } catch (e) {
      isLoading(false);
      notifyError(e);
    }
  };

  const columns = [
    {
      title: "Titulo",
      dataIndex: "title",
      key: "title",
      width: "20%",
    },
    {
      title: "Resumo",
      key: "spotlight",
      width: "70%",
      render: (data) => (
        <>
          <small>
            {formattedDate(
              firestoreTimeToMoment(data.createdAt),
              "DD MMM YYYY - hh:mm A"
            )}
          </small>
          <Paragraph type="secondary" ellipsis={{ rows: 2, expandable: true }}>
            {data.spotlight}
          </Paragraph>
        </>
      ),
    },
    {
      title: "Estado",
      dataIndex: "stateDetails.uf",
      key: "stateDetails.uf",
    },
    {
      title: "Ações",
      key: "actions",
      render: (news) => (
        <>
          <EditNewsForm news={news} />
          <DeleteNewsButton news={news} onDelete={getNews} />
        </>
      ),
    },
  ];

  const StateFilter = () => (
    <Select
      placeholder="Filtrar por estado"
      style={{ width: 220 }}
      onChange={setStateFilter}
      showSearch
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {states.map((state) => (
        <Option
          key={state.id}
          value={state.id}
        >{`${state.name}-${state.uf}`}</Option>
      ))}
    </Select>
  );

  return (
    <div>
      <Row type="flex" gutter={24} align="middle">
        <Col span={4}>
          <Can perform="news:see_all" yes={() => <StateFilter />} />
        </Col>
        <Col style={{ display: "flex" }} span={4}>
          <Paragraph style={{ marginRight: 10 }} level={4}>
            Destaques
          </Paragraph>
          <Switch
            checked={highlightsFilter}
            onChange={() => setHighlightsFilter(!highlightsFilter)}
          />
        </Col>
      </Row>
      <Row gutter={24}>
        <Col>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={news}
            loading={loading}
            rowExpandable
            expandedRowRender={(news) => <UserDetails news={news} />}
          />
        </Col>
      </Row>
    </div>
  );
};

const UserDetails = ({ news }) => {
  return (
    <Row gutter={16}>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Notícia
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Título">{news.title}</Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Slug">{news.slug}</Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Resumo">{news.spotlight}</Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Texto">
            <div dangerouslySetInnerHTML={{ __html: news.text }} />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions layout="vertical" bordered size="small">
          {news.categoryDetails && (
            <Descriptions.Item label="Categoria">
              <Tag color={news.categoryDetails.color}>
                {news.categoryDetails.name}
              </Tag>
            </Descriptions.Item>
          )}
          <Descriptions.Item label="Imagem de Destaque">
            <img src={news.image} height={300} alt="" />
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col>
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          Autor
        </Divider>
        <Descriptions layout="vertical" bordered size="small">
          <Descriptions.Item label="Nome">{news.author.name}</Descriptions.Item>
          <Descriptions.Item label="UF/Cidade">
            {news.author.stateUf} - {news.author.cityName}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  );
};

const DeleteNewsButton = ({ news, onDelete }) => {
  const [loading, setLoading] = useState(false);

  const deleteNews = async () => {
    try {
      setLoading(true);
      await deleteNewsService(news.id);
      onDelete();
      notifySuccess({ title: "Tudo certo", message: "A notícia foi excluída" });
      setLoading(false);
    } catch (e) {
      setLoading(false);
      notifyError(e);
    }
  };

  return (
    <Popconfirm
      title={
        <p>
          Você tem quer deletar mesmo? <br /> Esta operação não poderá ser
          desfeita
        </p>
      }
      onConfirm={deleteNews}
      okText="Sim"
      cancelText="Não"
    >
      <Tag color="red" style={{ marginTop: 5 }}>
        <Icon type={loading ? "loading" : "close"} /> Excluir
      </Tag>
    </Popconfirm>
  );
};
