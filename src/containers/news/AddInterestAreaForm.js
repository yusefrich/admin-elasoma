import React, { useState } from "react";
import {
  Button,
  Col,
  Drawer,
  Form,
  Icon,
  Input,
  message,
  Row,
  Upload,
} from "antd";
import { SliderPicker } from "react-color";

import {
  deleteImageFromUrlService,
  uploadFileService,
} from "../../services/storage";
import { getUserFromStore } from "../../helpers/store";
import { notifyError, notifySuccess } from "../../helpers/notifications";
import { createNewsService } from "../../services/news";
import { createNewAreaOfInterestService } from "../../services/areasOfInterests";

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error(
      "Você só pode realizar o upload de imagem do tipo JPG ou PNG"
    );
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Imagem deve ser menor que 2MB!");
  }
  return isJpgOrPng && isLt2M;
}

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

export default Form.create({ name: "add_news_form" })(
  ({
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
      validateFields,
      getFieldValue,
      resetFields,
    },
  }) => {
    const [drawerOpen, isDrawerOpen] = useState(false);
    const [loading, isLoading] = useState(false);
    const [buttonDisabled, isButtonDisabled] = useState(false);
    const [uploadingImage, isUploadingImage] = useState(false);
    const [color, setColor] = useState("");
    const [imageUrl, setImageUrl] = useState("");

    const user = getUserFromStore();

    const handleSubmit = (e) => {
      e.preventDefault();
      validateFieldsAndScroll(async (err, values) => {
        if (!err) {
          try {
            isLoading(true);
            values.image = imageUrl;
            values.color = values.color.hex;

            await createNewAreaOfInterestService(values);
            notifySuccess({
              title: "Ok!",
              message: "Uma nova área de interesse foi criada ",
            });
            resetFields();
            setImageUrl("");
            isLoading(false);
          } catch (e) {
            isLoading(false);
            notifyError(e);
          }
        }
      });
    };

    const onUpload = ({ onSuccess, onError, file, onProgress }) => {
      const onSuccessCallback = (url) => {
        setImageUrl(url);
        onSuccess("done");
      };
      uploadFileService({
        url: "areas_of_interests",
        onSuccessCallback,
        onError,
        file,
        onProgress,
      });
    };

    const handleChangeImage = (info) => {
      if (info.file.status === "uploading") {
        isUploadingImage(true);
        isButtonDisabled(true);
        setImageUrl("");
        return;
      }
      if (info.file.status === "done") {
        isButtonDisabled(false);
        isUploadingImage(false);
      }
    };

    const onCancelForm = async () => {
      if (imageUrl) deleteImageFromUrlService(imageUrl);
      isDrawerOpen(false);
      setImageUrl("");
    };

    return (
      <>
        <Button type="primary" onClick={() => isDrawerOpen(!drawerOpen)}>
          <Icon type="plus" /> Nova Área de Interesse
        </Button>
        <Drawer
          title="Criar Área de Interesse"
          width={520}
          onClose={onCancelForm}
          visible={drawerOpen}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form onSubmit={handleSubmit} layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item label="Titulo">
                  {getFieldDecorator("name", {
                    rules: [{ required: true, message: "Insira um nome" }],
                  })(<Input />)}
                </Form.Item>
                <Form.Item label="Cor de Destaque">
                  {getFieldDecorator("color", {
                    rules: [{ required: true, message: "Selecione uma cor" }],
                  })(
                    <SliderPicker
                      color={color}
                      onChange={({ hex }) => setColor(hex)}
                    />
                  )}
                </Form.Item>
                <Form.Item label="Imagem de Destaque">
                  {getFieldDecorator("image", {
                    rules: [{ required: true, message: "Insira uma imagem" }],
                  })(
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                      customRequest={onUpload}
                      onChange={handleChangeImage}
                    >
                      {imageUrl ? (
                        <img
                          src={imageUrl}
                          alt="avatar"
                          style={{ width: "100%" }}
                        />
                      ) : (
                        <div>
                          <Icon type={uploadingImage ? "loading" : "plus"} />
                          <div className="ant-upload-text">Upload</div>
                        </div>
                      )}
                    </Upload>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Button onClick={onCancelForm} style={{ marginRight: 8 }}>
                Cancelar
              </Button>
              <Button
                disabled={buttonDisabled}
                loading={loading}
                htmlType="submit"
                type="primary"
              >
                Salvar
              </Button>
            </div>
          </Form>
        </Drawer>
      </>
    );
  }
);
