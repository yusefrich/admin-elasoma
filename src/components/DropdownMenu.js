import React from "react";
import { Icon, Menu, Dropdown } from "antd";

export default ({ options, label }) => {
  const menu = (
    <Menu>
      {options.map(({ onSelect, label }) => (
        <Menu.Item onClick={onSelect}>{label}</Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
        {label} <Icon type="down" />
      </a>
    </Dropdown>
  );
};
