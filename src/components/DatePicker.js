import React from "react";
import { DatePicker } from "antd";
import locale from "antd/es/date-picker/locale/pt_BR";
import moment from "moment";
import "moment/locale/pt";
moment.locale("pt");

const DEFAULT_DATE_FORMAT = "DD/MM/YYYY";

export default ({ defaultValue, format = DEFAULT_DATE_FORMAT, ...rest }) => (
  <DatePicker
    defaultValue={defaultValue && moment(defaultValue, format)}
    format={format}
    locale={locale}
    {...rest}
  />
);
