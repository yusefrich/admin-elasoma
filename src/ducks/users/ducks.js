import {createReducer} from 'reduxsauce'

export const INITIAL_STATE = { user: {}};


export const HANDLERS = {
};

export default createReducer(INITIAL_STATE, HANDLERS)
