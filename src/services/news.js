import { getColDataWhere, getColRef, getDocRef } from "./firestore";
import { getSingleAreaOfInterestsService } from "./interests";
import { getSingleState } from "./state";
import { userCan } from "../rbac/Can";
import { getUserFromStore } from "../helpers/store";
import { firestore } from "./firebase";

const NEWS_ENDPOINT = "news/";

export const createNewsService = async (values) => {
  values.categoryDetails = await getSingleAreaOfInterestsService(
    values.category
  );
  values.category = [values.category];
  values.stateDetails = await getSingleState(values.state);
  values.likesQuantity = 0;

  const newsRef = getColRef(NEWS_ENDPOINT, {});
  return await newsRef.add(values);
};

export const updateNewsService = async (newsId, values) => {
  values.categoryDetails = await getSingleAreaOfInterestsService(
    values.category
  );
  values.category = [values.category];
  values.stateDetails = await getSingleState(values.state);

  const newsRef = getDocRef(`${NEWS_ENDPOINT}${newsId}`);
  return await newsRef.update(values);
};

export const updateLikesService = async (newsId, likes) => {
  const newsRef = getDocRef(`${NEWS_ENDPOINT}${newsId}`);
  return await newsRef.update({ likes, likesQuantity: likes.length || 0 });
};

export const deleteNewsService = async (newsId) => {
  const newsRef = getDocRef(`${NEWS_ENDPOINT}${newsId}`);
  return await newsRef.delete();
};

export const getNewsService = async ({ filters: { stateId, highlight } }) => {
  const user = getUserFromStore();

  if (!userCan({ perform: "news:see_all" })) {
    stateId = user._state._id;
  }

  let ref = firestore.collection("news");

  if (stateId !== null) ref = ref.where("state", "==", stateId);
  if (highlight) ref = ref.where("highlight", "==", highlight);

  const snapshot = await ref.orderBy("createdAt", "desc").get();

  const data = [];
  snapshot.docs.forEach((item) => data.push({ id: item.id, ...item.data() }));

  return data;
};

export const getNewsByStateService = async (stateId) => {
  return await getColDataWhere("news", {
    orderBy: "createdAt",
    orderDirection: "desc",
    where: { field: "state", operator: "==", value: stateId },
  });
};
