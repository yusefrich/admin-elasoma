import { firestore } from "./firebase";

const SUBSCRIBERS_PATH = "users";

export const createNewsletterSubscribersListener = (
  { filters: { stateId }, startAfter },
  callback
) => {
  let colPath = firestore.collection(SUBSCRIBERS_PATH);

  colPath = colPath.where("isSubscribedToNewsletter", "==", true);

  if (stateId) colPath = colPath.where("state", "==", stateId);
  if (startAfter) colPath.startAfter(startAfter);

  colPath.orderBy("name", "desc");

  colPath.onSnapshot((snapshot) => {
    const data = [];
    snapshot.docs.forEach((item) => data.push({ id: item.id, ...item.data() }));

    callback(data);
  });
};
