import { getColRef } from "./firestore";

const ENDPOINT = "areas_of_interest";

export const createNewAreaOfInterestService = async (values) => {
  const newsRef = getColRef(ENDPOINT, {});
  return await newsRef.add(values);
};
