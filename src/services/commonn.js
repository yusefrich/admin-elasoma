import { firebaseConfig } from "./firebase";

export const getAddressService = async (query) => {
  const request = await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?key=${firebaseConfig.apiKey}&address=${query}`
  );
  const { results } = await request.json();

  return results[0];
};
