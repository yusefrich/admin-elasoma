const { getUserService } = require("../services/users");
const { firestore } = require("../services/firebase");

const FORUM_CATEGORIES_PATH = "forum_categories";
const FORUM_PATH = "forum";

exports.updateForumCategory = async function (data, context) {
  try {
    const { uid } = context.auth;
    const { category, newData } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const forumPosts = await getForumPostsByByCategory(category.id);

    var batch = firestore.batch();

    for (const singlePost of forumPosts) {
      const forumPostRef = firestore.doc(`${FORUM_PATH}/${singlePost.id}`);
      batch.update(forumPostRef, {
        category: { ...singlePost.category, ...newData },
      });
    }

    const forumCategoryRef = firestore.doc(
      `${FORUM_CATEGORIES_PATH}/${category.id}`
    );
    batch.update(forumCategoryRef, newData);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

exports.deleteForumCategory = async function (data, context) {
  try {
    const { uid } = context.auth;
    const { category } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const forumPosts = await getForumPostsByByCategory(category.id);

    var batch = firestore.batch();

    for (const singleForumPost of forumPosts) {
      const singleForumPostRef = firestore.doc(
        `${FORUM_PATH}/${singleForumPost.id}`
      );
      batch.update(singleForumPostRef, {
        category: null,
      });
    }

    const forumCategory = firestore.doc(
      `${FORUM_CATEGORIES_PATH}/${category.id}`
    );
    batch.delete(forumCategory);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

async function getForumPostsByByCategory(categoryId) {
  const discountsRef = firestore
    .collection(FORUM_PATH)
    .where("category.id", "==", categoryId)
    .orderBy("createdAt");
  const discounts = await discountsRef.get();

  return discounts.docs.map((n) => ({ ...n.data(), id: n.id }));
}
