const functions = require("firebase-functions");
const {
  createEventGalleryThumbnail,
} = require("./Events/createEventGalleryThumbnail");
const {
  sendEmailOnUserCreation,
  sendEmailOnUserApproval,
  scheduledEmailWithValidityDate,
} = require("./Emails");
const {
  createUser,
  deleteUserDataWhenDeleteUser,
  deleteUser,
  updateUserData,
} = require("./Users");
const {
  updateAreaOfInterest,
  deleteAreaOfInterest,
} = require("./AreasOfInterests");

const {
  updateDiscountClubCategory,
  deleteDiscountClubCategory,
} = require("./DiscountClub");

const {
  notifyOnEventCreation,
  notifyOnForumPostCreation,
  notifyOnReplyForumPost,
  eventsRemiderListenerNotification,
  eventsThreeDaysReminderListenerNotification,
} = require("./Notifications");

const { onCreateNews } = require("./News");

const { scheduledFirestoreExport } = require("./App");

const { deleteForumCategory, updateForumCategory } = require("./Forum");

exports.createUser = functions.https.onCall(async (data, context) => {
  return await createUser(data, context);
});

exports.updateAreaOfInterest = functions.https.onCall(async (data, context) => {
  return await updateAreaOfInterest(data, context);
});

exports.deleteAreaOfInterest = functions.https.onCall(async (data, context) => {
  return await deleteAreaOfInterest(data, context);
});

exports.deleteUserDataWhenDeleteUser = deleteUserDataWhenDeleteUser;
exports.deleteUser = deleteUser;
exports.updateUserData = updateUserData;

//Discount Club
exports.updateDiscountClubCategory = functions.https.onCall(
  async (data, context) => {
    return await updateDiscountClubCategory(data, context);
  }
);
exports.deleteDiscountClubCategory = functions.https.onCall(
  async (data, context) => {
    return await deleteDiscountClubCategory(data, context);
  }
);

//Forum
exports.updateForumCategory = functions.https.onCall(async (data, context) => {
  return await updateForumCategory(data, context);
});
exports.deleteForumCategory = functions.https.onCall(async (data, context) => {
  return await deleteForumCategory(data, context);
});

//Notifications
if (process.env.NODE_ENV !== "development") {
  exports.notifyOnEventCreation = notifyOnEventCreation;
  exports.notifyOnForumPostCreation = notifyOnForumPostCreation;
  exports.notifyOnReplyForumPost = notifyOnReplyForumPost;
  exports.eventsRemiderListenerNotification = eventsRemiderListenerNotification;
  exports.eventsThreeDaysReminderListenerNotification = eventsThreeDaysReminderListenerNotification;
}

//NEWS
exports.onCreateNews = onCreateNews;

//APP
exports.scheduledFirestoreExport = scheduledFirestoreExport;

//EMAILS
if (process.env.NODE_ENV !== "development") {
  exports.sendEmailOnUserCreation = sendEmailOnUserCreation;
}
exports.sendEmailOnUserApproval = sendEmailOnUserApproval;
exports.scheduledEmailWithValidityDate = scheduledEmailWithValidityDate;

//Events
exports.createEventGalleryThumbnail = createEventGalleryThumbnail;
