const moment = require("moment");
require("moment/locale/pt");
var slugify = require("slugify");

const { getColWhereData, updateDoc } = require("../services/firestore");
const { functions } = require("../services/firebase");

moment.locale("pt");

exports.onCreateNews = functions.firestore
  .document("news/{newsId}")
  .onCreate(async (snap, context) => {
    const data = snap.data();
    const { newsId } = context.params;

    try {
      const slug = await createSlug(data);

      await updateDoc(`news/${newsId}`, { slug });
    } catch (e) {
      console.log(
        `Error ao criar slug para a notícia ${newsId}. Razão: ${e.message} `
      );
    }
  });

const createSlug = async (data) => {
  const slugifyString = (string) => {
    return slugify(string, {
      replacement: "_",
      lower: true,
    });
  };

  let slug = slugifyString(data.title);
  let isSlugApproved = false;
  let counter = 0;

  while (isSlugApproved === false) {
    const newsWithSameSlug = await getColWhereData("news", {
      where: { field: "slug", value: slug, operator: "==" },
    });

    if (newsWithSameSlug.length <= 0) isSlugApproved = true;
    else {
      slug = slugifyString(`${data.title}_${counter}`);
      counter++;
    }
  }

  return slug;
};
