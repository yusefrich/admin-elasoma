const { auth } = require("./firebase");

exports.createAuthUserService = async function(data) {
  return await auth.createUser(data);
};
