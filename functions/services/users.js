const { getDocRef, getDoc, updateDoc } = require("./firestore");

const USERS_PATH = "users";
const getUserPath = (userId) => `${USERS_PATH}/${userId}`;

exports.getUserService = async function (userId) {
  const user = await getDocRef(getUserPath(userId)).get();
  return { id: user.id, ...user.data() };
};

exports.updateUserService = async function (userId, data) {
  if (data.city && data.state) {
    const city = await getDoc(`states/${data.state}/cities/${data.city}`);
    data.cityName = city.name;
  } else {
    data.cityName = "";
    data.city = "";
  }

  if (data.state) {
    const state = await getDoc(`states/${data.state}`);
    data.stateUf = state.uf;
  } else {
    data.state = "";
    data.stateUf = "";
  }
  return await updateDoc(getUserPath(userId), data);
};
