const { firestore } = require("./firebase");

exports.getDocRef = function (path) {
  return firestore.doc(path);
};

exports.getDoc = async function (path) {
  const data = await firestore.doc(path).get();
  return { ...data.data(), id: data.id };
};

exports.getColWhereRef = async function (
  path,
  { where: { field, operator, value } }
) {
  return firestore.collection(path).where(field, operator, value);
};

exports.getColWhereData = async function (
  path,
  { where: { field, operator, value } }
) {
  const col = await firestore
    .collection(path)
    .where(field, operator, value)
    .get();

  const data = [];

  col.forEach((item) => data.push({ id: item.id, ...item.data() }));

  return data;
};

exports.getColRef = function (path, { orderBy }) {
  if (orderBy) {
    return firestore.collection(path).orderBy(orderBy);
  }
  return firestore.collection(path);
};

exports.getColData = async function (path, { orderBy }) {
  try {
    let ref = firestore.collection(path);
    if (orderBy) {
      ref = firestore.collection(path).orderBy(orderBy);
    }

    const col = await ref.get();
    const data = [];

    col.forEach((item) => data.push({ id: item.id, ...item.data() }));

    return data;
  } catch (e) {
    throw e;
  }
};

exports.addDoc = async function (collectionPath, data) {
  try {
    const ref = firestore.collection(collectionPath);
    return await ref.add(data);
  } catch (e) {
    throw e;
  }
};

exports.setDoc = async function (docPath, data) {
  try {
    const ref = firestore.doc(docPath);
    return await ref.set(data);
  } catch (e) {
    throw e;
  }
};

exports.updateDoc = async function (docPath, data) {
  try {
    const ref = firestore.doc(docPath);
    return await ref.update(data);
  } catch (e) {
    throw e;
  }
};

exports.deleteCollection = async function (path) {
  try {
    const batch = firestore.batch();
    const snapshot = await firestore.collection(path).get();

    if (snapshot.size === 0) {
      return 0;
    }

    snapshot.docs.forEach((doc) => {
      batch.delete(doc.ref);
    });

    return await batch.commit();
  } catch (e) {
    throw e;
  }
};
