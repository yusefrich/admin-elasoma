const { sendEmail } = require("../services/email");
const { functions, firestore } = require("../services/firebase");

const today = new Date();
const futureDate = new Date();
futureDate.setDate(futureDate.getDate() + 3);

exports.scheduledEmailWithValidityDate = functions.pubsub
  .schedule("every 24 hours")
  .onRun(async () => {
    try {
      const emailsPromise = [];
      const expiredUsersCol = await firestore
        .collection("users")
        .where("validity", ">", today)
        .where("validity", "<", futureDate)
        .get();

      expiredUsersCol.forEach((item) => {
        const user = { id: item.id, ...item.data() };

        const emailData = {
          to: "adm@sejasoma.com.br",
          attachments: [
            {
              filename: "negative_brand.png",
              path: __dirname + "/../assets/negative_brand.png",
              cid: "negative_brand",
            },
          ],
          subject: "Boas Vindas, seu cadastro foi Aprovado - Seja Soma",
          html: `<h1>Olá, Administrador.</h1>
                <h4>O filiado ${user.name} está perto de vencer</h4>
                <p>Caso o cliente realizou a renovação, altere a data no gerenciador.</p>
                <div style="margin-top: 30px">
                    <a href="https://app.sejasoma.com.br/dashboard/users/${user.id}" target="_blank">Clique aqui para acessar o gerenciador</a>
                </div>
                
                <div style="margin-top: 50px">
                    <img src="cid:negative_brand" alt="">
                </div>
                <div>
                    <p>Essa plataforma foi orgulhosamente desenvolvida pela <a href="https://www.qualitare.com/" target="_blank">Qualitare</a></p>
                </div>
`,
        };
        console.log(`Vai email email sobre ${user.email}`);
        emailsPromise.push(sendEmail(emailData));
      });

      if (emailsPromise.length > 0) {
        return await Promise.all(emailsPromise);
      }

      console.log("Nenhum email foi enviado");

      return {};
    } catch (e) {
      console.log("Houve um erro");
      console.log(e.message);
    }
  });
