const {
  scheduledEmailWithValidityDate,
} = require("./sendDaillyEmailWithValidityUsers");
const { sendEmailOnUserCreation } = require("./sendEmailOnUserCreation");
const { sendEmailOnUserApproval } = require("./sendEmailOnUserApproval");

exports.sendEmailOnUserCreation = sendEmailOnUserCreation;
exports.sendEmailOnUserApproval = sendEmailOnUserApproval;
exports.scheduledEmailWithValidityDate = scheduledEmailWithValidityDate;
