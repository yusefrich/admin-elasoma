const { functions, messaging } = require("../services/firebase");
require("moment/locale/pt");
const { notificationTypes } = require("./notificationTypes");

exports.notifyOnForumPostCreation = functions.firestore
  .document("forum/{postId}")
  .onWrite(async (change, context) => {
    try {
      const newData = change.after.data();
      const oldData = change.before.data();

      if (newData.isActive && (!oldData || !oldData.updatedAt)) {
        const notification = {
          notification: {
            title: "Novo fórum no ar, participe!",
            body: newData.title,
          },
          android: {
            notification: {
              sound: "default",
              tag: "forumPostCreation",
            },
          },
          apns: {
            payload: {
              aps: {
                sound: "default",
              },
            },
          },
          data: {
            type: notificationTypes.NEW_FORUM_POST,
            postId: context.params.postId,
          },
          topic: `newForumPostNotifications${newData.stateDetails.id}`,
        };

        const notificationId = await messaging.send(notification);
        console.log(
          "Notificação para o tópico",
          `NewForumPostNotification${newData.stateDetails.id}`,
          "foi enviada com sucesso com o notificationId: ",
          notificationId
        );
      }
    } catch (e) {
      console.log("Error sending notification ao criar evento:", e);
    }
  });
