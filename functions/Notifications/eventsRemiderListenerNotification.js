const { functions, messaging, firestore } = require("../services/firebase");
const moment = require("moment");
require("moment/locale/pt");
const { notificationTypes } = require("./notificationTypes");
moment.locale("pt");

exports.eventsRemiderListenerNotification = functions.pubsub
  .schedule("every day 12:00")
  .onRun(async (context) => {
    try {
      const startMoment = new moment();

      const events = await firestore
        .collection("events")
        .where("dateTime", ">=", startMoment.toDate())
        .where("dateTime", "<=", startMoment.add(2, "days").toDate())
        .orderBy("dateTime", "desc")
        .get();

      const notificationsToSend = events.docs.map((item) => {
        const event = { id: item.id, ...item.data() };
        const momentDate = new moment(event.dateTime.toDate());

        const notification = {
          notification: {
            title: event.title,
            body: momentDate.utcOffset("-03:00").calendar(null, {
              sameDay: "[Hoje às ]HH:mm",
              nextDay: "[Amanhã às ]HH:mm",
              nextWeek: "dddd",
              lastDay: "[Ontem]",
              lastWeek: "[Última] dddd",
              sameElse: "DD/MM/YYYY",
            }),
          },
          android: {
            notification: {
              sound: "default",
            },
          },
          apns: {
            payload: {
              aps: {
                sound: "default",
              },
            },
          },
          data: {
            type: notificationTypes.EVENTS,
            eventId: event.id,
          },
          topic: `eventsReminder${event.state}`,
        };

        return messaging.send(notification);
      });

      const notificationsIds = await Promise.all(notificationsToSend);

      console.log(`Lembrete de evento enviado para ${notificationsIds}`);

      return null;
    } catch (e) {
      console.log(e.message);
    }
  });
