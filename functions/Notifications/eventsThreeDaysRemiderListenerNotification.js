const { functions, messaging, firestore } = require("../services/firebase");
const { getColData } = require("../services/firestore");

const moment = require("moment");
require("moment/locale/pt");
const { notificationConfig } = require("./notificationTypes");
const { notificationTypes } = require("./notificationTypes");
moment.locale("pt");

exports.eventsThreeDaysReminderListenerNotification = exports.eventsThreeDaysReminderListenerNotification = functions.pubsub
  .schedule("every 72 hours")
  .onRun(async (context) => {
    try {
      const now = new moment();
      const states = await getColData("states", {});
      const notifications = [];

      for (const state of states) {
        const stateId = state.id;

        const eventsFromStatePromise = firestore
          .collection("events")
          .where("dateTime", ">=", now.toDate())
          .where("state", "==", stateId)
          .orderBy("dateTime", "desc")
          .get();

        const usersFromStatePromise = firestore
          .collection("users")
          .where("state", "==", stateId)
          .get();

        const [eventsFromState, usersFromState] = await Promise.all([
          eventsFromStatePromise,
          usersFromStatePromise,
        ]);

        for (const event of eventsFromState.docs) {
          const eventData = { id: event.id, ...event.data() };
          const eventDate = new moment(eventData.dateTime.toDate());
          const eventConfirmedPeople = eventData.confirmedIds || [];
          const eventDeniedPeople = eventData.deniedIds || [];

          const usersToSendNotification = usersFromState.docs.filter((user) => {
            const userData = { id: user.id, ...user.data() };
            return (
              !eventConfirmedPeople.includes(userData.id) &&
              !eventDeniedPeople.includes(userData.id) &&
              userData.futureEventsReminderSubscription === true
            );
          });

          notificationConfig.notification = {
            title: `Não fique de fora! Confirme sua participação no evento: ${eventData.title}`,
            body: eventDate.utcOffset("-03:00").calendar(null, {
              sameDay: "[Hoje às ]HH:mm",
              nextDay: "[Amanhã às ]HH:mm",
              nextWeek: "dddd",
              lastDay: "[Ontem]",
              lastWeek: "[Última] dddd",
              sameElse: "DD/MM/YYYY",
            }),
          };
          notificationConfig.data = {
            type: notificationTypes.EVENTS,
            eventId: eventData.id,
          };

          notificationConfig.tokens = [];

          for (const user of usersToSendNotification) {
            const userData = { id: user.id, ...user.data() };
            console.log(
              `Notificação será enviada para ${userData.id} - ${userData.name}`
            );
            if (
              userData.notificationTokens &&
              userData.notificationTokens.length > 0
            ) {
              userData.notificationTokens.forEach(({ token }) =>
                notificationConfig.tokens.push(token)
              );
            }
          }

          if (notificationConfig.tokens.length > 0) {
            notifications.push(
              messaging
                .sendMulticast(notificationConfig)
                .then(() =>
                  console.log(
                    `Notificação do evento: ${eventData.title} disparada para ${notificationConfig.tokens.length} dispositivos`
                  )
                )
            );
          }
        }
      }

      if (notifications.length > 0) {
        await Promise.all(notifications);
        console.log("Notificações de eventos enviadas");
      } else {
        console.log(
          "A função que envia lembretes a cada 3 dias foi executada, mas nenhuma notificação foi disparada"
        );
      }
    } catch (e) {
      console.log(e.message);
    }
  });
