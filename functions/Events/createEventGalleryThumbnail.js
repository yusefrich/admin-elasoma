const { functions, storageBucket } = require("../services/firebase");
const imageThumbnail = require("image-thumbnail");
const UUID = require("uuid-v4");
const { updateDoc } = require("../services/firestore");

exports.createEventGalleryThumbnail = functions.firestore
  .document("events/{eventId}/gallery/{imageId}")
  .onCreate(async (snap, context) => {
    try {
      const data = snap.data();
      const { eventId, imageId } = context.params;
      let uuid = UUID();

      const thumbnail = await imageThumbnail({ uri: data.url });
      const file = storageBucket.file(
        `events/${eventId}/gallery/thumb_${data.fileName}`
      );
      await file.save(thumbnail, {
        contentType: "auto",
        public: true,
        metadata: {
          public: true,
          firebaseStorageDownloadTokens: uuid,
        },
      });

      const fileData = await storageBucket
        .file(`events/${eventId}/gallery/thumb_${data.fileName}`)
        .getMetadata();

      await updateDoc(`events/${eventId}/gallery/${imageId}`, {
        thumbnail: fileData[0].mediaLink,
      });
    } catch (e) {
      console.log(e.message);
    }
  });
