const { getUserService } = require("../services/users");
const { firestore } = require("../services/firebase");

exports.updateAreaOfInterest = async function(data, context) {
  try {
    const { uid } = context.auth;
    const { area, newData } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const news = await getNewsByCategory(area.id);

    var batch = firestore.batch();

    for (const singleNews of news) {
      const newsRef = firestore.doc(`news/${singleNews.id}`);
      batch.update(newsRef, {
        category: area.id,
        categoryDetails: { ...singleNews.categoryDetails, ...newData }
      });
    }

    const areaRef = firestore.doc(`areas_of_interest/${area.id}`);
    batch.update(areaRef, newData);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

exports.deleteAreaOfInterest = async function(data, context) {
  try {
    const { uid } = context.auth;
    const { area } = data;
    const requestUserData = await getUserService(uid);

    if (requestUserData.role !== "SUPER_ADMIN")
      throw { message: "Você não pode processar esta solicitação" };

    const news = await getNewsByCategory(area.id);

    var batch = firestore.batch();

    for (const singleNews of news) {
      const newsRef = firestore.doc(`news/${singleNews.id}`);
      batch.update(newsRef, {
        category: null,
        categoryDetails: null
      });
    }

    const areaRef = firestore.doc(`areas_of_interest/${area.id}`);
    batch.delete(areaRef);

    await batch.commit();

    return { code: 200, message: "Dados Atualizados" };
  } catch (e) {
    return { code: e.code || 204, message: e.message };
  }
};

async function getNewsByCategory(categoryId) {
  const newsRef = firestore
    .collection("news")
    .where("category", "==", categoryId)
    .orderBy("createdAt");
  const news = await newsRef.get();

  return news.docs.map(n => ({ ...n.data(), id: n.id }));
}
